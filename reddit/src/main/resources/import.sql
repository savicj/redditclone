insert into users(user_role, id, avatar, description, display_name, email, password, registration_date, username) values("MODERATOR", 1, null, "opis", "Marko Markovic", "markan@mail.com", "$2a$12$p.gTuNyLq0BjvBx/6j5KDuRTDihFLQHTZphq/RaKvOEnkg5j26ryi", "2022-02-02", "markan");
insert into users(user_role, id, avatar, description, display_name, email, password, registration_date, username) values("ADMINISTRATOR", 2, null, "opis", "Pera Peric", "admin@mail.com", "$2a$12$iLM3sivmnYZbijpromCuDuutljQoqrs.3gRgzzcQTMAEoYWs4xDRm", "2022-02-03", "admin");
insert into users(user_role, id, avatar, description, display_name, email, password, registration_date, username) values("USER", 3, null, "opis", "Manja Mijatov", "banned@mail.com", "$2a$12$xY3ECE6xG.EFw185otoL8OLHCHIRdtkUqLlnf/pMCGEU7rHfk3Agi", "2021-08-10", "banned");
insert into users(user_role, id, avatar, description, display_name, email, password, registration_date, username) values("MODERATOR", 4, null, "personal descriptio", "Andrea Martin", "andy@mail.com", "$2a$12$p.gTuNyLq0BjvBx/6j5KDuRTDihFLQHTZphq/RaKvOEnkg5j26ryi", "2022-03-02", "user");

insert into community values (1, "Keep up with and discuss latest events of NBA league", false, "NBA community", null, null);
insert into community values (2, "Community for sharing healthy recepies, excercises and more!", false, "Health", null, null);
insert into community values (3, "desc suspendovana", true, "suspendovana zajednica", null, "test");
insert into community values (4, "Share delicious recepies with people across the world", false, "Foodie", null, null);
insert into community values (5, "Safe space for fangirling", false, "Tv shows|Movies|Books", null, null);


insert into communities_members values(1,1);
insert into communities_members values(1,2);
insert into communities_members values(1,3);
insert into communities_members values(2,1);
insert into communities_members values(2,2);
insert into communities_members values(3,1);
insert into communities_members values(4,1);
insert into communities_members values(4,2);
insert into communities_members values(5,4);
insert into communities_members values(5,4);
insert into communities_members values(5,1);


insert into communities_moderators values(1,1);
insert into communities_moderators values(2,1);
insert into communities_moderators values(2,4);
insert into communities_moderators values(4,4);
insert into communities_moderators values(5,4);

insert into post values(1, "2022-01-01", null, null, "UFA - Unrestricted Free Agent RFA - Restricted Free Agent ", "2022-23 NBA Free Agents and Team Roster Tracker", 1, 1, null);
insert into post values(2, "2022-02-01", null, null, "[Wojnarowski] The Indiana Pacers are trading guard Malcolm Brogdon to the Boston Celtics, sources tell ESPN.", "Title", 2, 1, null);
insert into post values(3, "2022-03-01", null, null, "Can anyone share their favorite or go to healthy breakfast? I need about 30g of protein atleast", "Bored of eggs every morning", 2, 2, null);
insert into post values(4, "2022-04-01", null, null, "I am looking for a large list of recipes, mainly for dinner, that are quick, easy to prepare, and relatively inexpensive. Any and all help is much appreciated.", "Looking for Large Healthy, Quick Meal List/Database", 3, 2, null);
insert into post values(5, "2022-05-01", null, null, "Hi healthy foodies! I’m a clean, healthy eater with a question. I crave cheese. A lot. I find myself snacking on it more than I want to. I wondered if anyone had suggestions on good substitutes?", "I just love cheese!", 1, 2, null);
insert into post values(6, "2022-06-01", null, null, "I am looking for suggestions for a healthier breakfast. I am on a low protein diet because of a medical condition. However I need a breakfast that will last me from 6:30 am to 1:00 pm", "Healthy low protein breakfast suggestions", 2, 4, null);
insert into post values(7, "2022-01-01", null, null, "It was amazing", "Made my first Cheesesteak ever tonight, fiancé approved! *chefs kiss*", 1, 4, null);
insert into post values(8, "2022-02-01", null, null, "THE WORST TV SHOW I'VE EVER SEEN IN MY LIFE.", "RIVERDALE", 2, 5, null);
insert into post values(9, "2022-03-01", null, null, "For me it's Dean from Supernatural. Klaus from The umbrella academy comes to second.", "What's your favourite TV show character?", 2, 5, null);
insert into post values(10, "2022-04-01", null, null, "Supernatural prequel series https://www.youtube.com/watch?v=E4w_hwnlv2k&feature=youtu.be", "The Winchesters (The CW) Trailer HD ", 3, 5, null);

insert into comment values(1, 0, "tekst", "2022-02-01", 2, 1, null);

insert into reaction values(1, 1, "2022-02-02", null, 1, 2);
insert into reaction values(2, 0, "2022-02-02", null, 1, 4);
insert into reaction values(3, 0, "2022-02-02", null, 1, 3);
insert into reaction values(4, 1, "2022-02-02", null, 2, 3);
insert into reaction values(5, 1, "2022-02-02", null, 2, 4);
insert into reaction values(6, 1, "2022-02-02", null, 3, 3);
insert into reaction values(7, 1, "2022-02-02", null, 3, 2);
insert into reaction values(8, 1, "2022-02-02", null, 3, 1);
insert into reaction values(9, 0, "2022-02-02", null, 4, 2);
insert into reaction values(10, 0, "2022-02-02", null, 4, 3);
insert into reaction values(11, 1, "2022-02-02", null, 5, 2);
insert into reaction values(12, 0, "2022-02-02", null, 6, 2);
insert into reaction values(13, 1, "2022-02-02", null, 6, 3);
insert into reaction values(14, 1, "2022-02-02", null, 7, 1);
insert into reaction values(15, 0, "2022-02-02", null, 8, 1);

insert into reaction values(16, 1, "2022-02-02", null, 8, 2);
insert into reaction values(17, 1, "2022-02-02", null, 8, 2);
insert into reaction values(18, 1, "2022-02-02", null, 9, 4);
insert into reaction values(19, 0, "2022-02-02", null, 10, 1);
insert into reaction values(20, 0, "2022-02-02", null, 10, 2);
insert into reaction values(21, 1, "2022-02-02", null, 10, 3);
insert into reaction values(22, 0, "2022-02-02", null, 10, 4);
