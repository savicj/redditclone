package ftn.sf.reddit.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.sf.reddit.model.Comment;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.Reaction;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.model.Reaction.ReactionType;

public interface ReactionRepository extends JpaRepository<Reaction, Long>{
    
    List<Reaction> findByReactedBy(User reactedBy);

    List<Reaction> findByComment(Comment comment);

    List<Reaction> findByPost(Post post);

    List<Reaction> findByTimestamp(LocalDate timestamp);

    List<Reaction> findByReactionType(ReactionType reactionType);
}
