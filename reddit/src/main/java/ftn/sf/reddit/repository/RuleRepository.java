package ftn.sf.reddit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Rule;

public interface RuleRepository extends JpaRepository<Rule, Long>{
    
    List<Rule> findByDescription(String description);

    List<Rule> findByCommunity(Community community);
}
