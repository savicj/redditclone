package ftn.sf.reddit.repository;


import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Flair;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.User;

public interface PostRepository extends JpaRepository<Post, Long> {

    @Query(value = "select p.* from post p left join community c on p.community_id=c.id where c.is_suspended=false;", nativeQuery = true)
    List<Post> findAll();

    Post findByTitle(String title);

    List<Post> findByCreationDate(LocalDate localDate);

    List<Post> findByAuthor(User byId);

    List<Post> findByFlair(Flair byId);

    List<Post> findByCommunity(Community byId);

    @Query(value = "SELECT ifnull(b.cnt, 0) - ifnull(a.cnt,0) as Cnt" +
    " FROM " +
    "(SELECT reacted_post, reaction_type, COUNT(reaction_type)  as cnt " +
    "FROM reaction " + 
    "WHERE reaction_type='1' AND reacted_post = ?1) a " +
    "left join " +
    "(SELECT reacted_post, reaction_type, COUNT(reaction_type)  as cnt " +
    "FROM reaction " +
    "WHERE reaction_type='0' AND reacted_post = ?2) b on a.reacted_post=b.reacted_post;", nativeQuery = true)
    Long calculateKarma(Long id, Long id2);
    
}
