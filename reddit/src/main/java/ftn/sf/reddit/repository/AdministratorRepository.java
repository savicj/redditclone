package ftn.sf.reddit.repository;

import org.springframework.data.jpa.repository.Query;

import ftn.sf.reddit.model.Administrator;

public interface AdministratorRepository extends UserRepository<Administrator> {

    Administrator findByUsername(String username);
    @Query(value = "update users set user_role = 'USER' where id = ?1", nativeQuery = true)
    void removeAdministrator(Long id);
    
}
