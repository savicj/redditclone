package ftn.sf.reddit.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.sf.reddit.model.Comment;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.Report;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.model.Report.ReportReason;

public interface ReportRepository extends JpaRepository<Report, Long>{

    List<Report> findByReason(ReportReason reason);

    List<Report> findByTimestamp(LocalDate timestamp);

    List<Report> findByByUser(User user);

    List<Report> findByAccepted(boolean accepted);

    List<Report> findByComment(Comment comment);

    List<Report> findByPost(Post byId);
    
}
