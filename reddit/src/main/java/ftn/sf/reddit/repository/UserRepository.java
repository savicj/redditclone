package ftn.sf.reddit.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ftn.sf.reddit.model.User;

// @NoRepositoryBean
public interface UserRepository<T extends User> extends JpaRepository<T, Long> {
    
    Optional<User> findFirstByUsername(String username);

    @Query(value = "SELECT k.user_role FROM users k WHERE k.id = ?1", nativeQuery = true)
    String getRole(Long id);

    // @Query(value = "update users set password = ?1, ")
    // void changePassword(String newp, String old);
    
    @Query(value = "SELECT b.cnt - a.cnt as Cnt" +
    " FROM " +
    "(SELECT reacted_by, reaction_type, COUNT(reaction_type)  as cnt " +
    "FROM reaction " + 
    "WHERE reaction_type='1' AND reacted_by = ?1) a " +
    "left join " +
    "(SELECT reacted_by, reaction_type, COUNT(reaction_type)  as cnt " +
    "FROM reaction " +
    "WHERE reaction_type='2' AND reacted_by = ?2) b on a.reacted_by=b.reacted_by;", nativeQuery = true)
    Long calculateKarma(Long id, Long id2);

    User findByUsername(String username);

    @Query(value = "SELECT u.username FROM users u left join communities_members cm on u.id = cm.user_id where community_id = ?1", nativeQuery = true)
    List<String> findMembers(Long id);
    //block

    //unblock

    //removemoderates
}
