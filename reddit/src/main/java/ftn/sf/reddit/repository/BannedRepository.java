package ftn.sf.reddit.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.sf.reddit.model.Banned;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Moderator;
import ftn.sf.reddit.model.User;

public interface BannedRepository extends JpaRepository<Banned, Long>{
    
    Banned findByUser(User user);
    List<Banned> findByBannedFrom(Community community);
    List<Banned> findByBy(Moderator by);
    List<Banned> findByTimestamp(LocalDate timestamp);
}
