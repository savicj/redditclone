package ftn.sf.reddit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Flair;

public interface FlairRepository extends JpaRepository<Flair, Long> {

    Flair findByName(String name);

    List<Flair> findByCommunity(Community community);
    
}
