package ftn.sf.reddit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Moderator;

public interface ModeratorRepository extends UserRepository<Moderator>{
    
    @Transactional
    @Query(value = "update users set user_role = 'USER' where id = ?1")
    void removeModerates(Long id);

    // @Query(value = "delete from communities_moderators where moderator_id = ?1")
    // void removeModeratorFromCommunities(Long mid);

    List<Moderator> findByModerates(Community community);
    
}
