package ftn.sf.reddit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.model.Comment;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.User;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByAuthor(User byId);

    List<Comment> findByRepliedTo(CommentDTO commentDTO);

    List<Comment> findByPost(Post byId);

    @Query(value = "SELECT b.cnt - a.cnt as Cnt" +
    " FROM " +
    "(SELECT reacted_comment, reaction_type, COUNT(reaction_type)  as cnt " +
    "FROM reaction " + 
    "WHERE reaction_type='1' AND reacted_by = ?1) a " +
    "left join " +
    "(SELECT reacted_comment, reaction_type, COUNT(reaction_type)  as cnt " +
    "FROM reaction " +
    "WHERE reaction_type='2' AND reacted_by = ?2) b on a.reacted_comment=b.reacted_comment;", nativeQuery = true)
    Long calculateKarma(Long id, Long id2);
    
}
