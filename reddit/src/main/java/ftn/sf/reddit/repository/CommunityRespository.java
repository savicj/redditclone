package ftn.sf.reddit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.sf.reddit.model.Banned;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Flair;
import ftn.sf.reddit.model.Moderator;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.User;

public interface CommunityRespository extends JpaRepository<Community, Long>{
    
    public List<Community> findByName(String name);
    public List<Community> findByIsSuspended(boolean isSuspended);
    public List<Community> findByModeratedBy(Moderator moderator);
    public Community findByPosts(Post post);
    public List<Community> findByMembers(User user);
    public List<Community> findByFlairs(Flair flair);
    public List<Community> findByBanned(Banned banned);
    

}
