package ftn.sf.reddit.lucene.esRepository;

import ftn.sf.reddit.lucene.elasticModel.PostES;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface PostRepositoryES extends ElasticsearchRepository<PostES, Long> {

    List<PostES> findByKarmaBetween(int min, int max);

    @Query("{ \"bool\":{\"must\":{\"range\":{\"karma\":{\"from\":null, \"to\":?0, \"include_lower\": true, \"include_upper\":true}}}}}")
    List<PostES> findByKarmaLessThan(int max);
    @Query("{ \"bool\":{\"must\":{\"range\":{\"karma\":{\"from\":?0, \"to\":null, \"include_lower\": true, \"include_upper\":true}}}}}")
    List<PostES> findByKarmaGreaterThan(int min);

    List<PostES> findAllByCommunity(Long community);

}
