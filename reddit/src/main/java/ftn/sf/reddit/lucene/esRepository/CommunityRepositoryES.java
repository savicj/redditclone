package ftn.sf.reddit.lucene.esRepository;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.lucene.elasticModel.CommunityES;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface CommunityRepositoryES extends ElasticsearchRepository<CommunityES, Long> {

    List<CommunityES> findAllByName(String name);

    List<CommunityES> findAllByDescription(String description);

    List<CommunityES> findByIsSuspended(boolean isSuspended);

    List<CommunityES> findAllByPdfDescription(String text);

}
