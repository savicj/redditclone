package ftn.sf.reddit.lucene.indexing.handlers;

import ftn.sf.reddit.lucene.elasticModel.CommunityES;
import ftn.sf.reddit.lucene.elasticModel.PostES;

import java.io.File;


public abstract class DocumentHandler {

	public abstract PostES getPostEs(File file);
	public abstract CommunityES getCommunityEs(File file);
	public abstract String getText(File file);

}
