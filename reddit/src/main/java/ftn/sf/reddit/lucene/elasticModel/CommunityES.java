package ftn.sf.reddit.lucene.elasticModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "communities")
@Setting(settingPath = "/analyzers/customAnalyzer.json")
public class CommunityES {

    @Id
    private Long id;
    private String name;
    @Field(type = FieldType.Text)
    private String description;
    @Field(type = FieldType.Boolean)
    private boolean isSuspended;
    @Field(type = FieldType.Text)
    private String suspendedReason;
    private String filename;
    @Field(type = FieldType.Text)
    private String pdfDescription;
    @Field(type = FieldType.Nested)
    private List<Long> members;
    @Field(type = FieldType.Nested)
    private List<Long> moderatedBy;
    @Field(type = FieldType.Nested)
    private List<PostES> posts;
    @Field(type = FieldType.Nested)
    private List<Long> flairs;
    @Field(type = FieldType.Nested)
    private List<Long> banned;


}
