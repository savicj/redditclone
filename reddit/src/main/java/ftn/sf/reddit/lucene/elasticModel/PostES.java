package ftn.sf.reddit.lucene.elasticModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import javax.persistence.Id;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(indexName = "posts")
@Setting(settingPath = "/analyzers/customAnalyzer.json")
public class PostES {
    @Id
    private Long id;
    private String title;
    @Field(type = FieldType.Text)
    private String text;
    private Long author;
    @Field(type = FieldType.Date)
    private LocalDate creationDate;
    private String imagePath;
    private Long flair;
    private Long community;
    @Field(type = FieldType.Nested)
    private List<Long> comments;
    private String filename;
    @Field(type = FieldType.Text)
    private String pdfText;
    int karma;
}
