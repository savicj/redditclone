package ftn.sf.reddit.lucene.search;

public enum SearchType {

		TERM,
		FUZZY,
		PHRASE,
		RANGE,
		PREFIX,
		MATCH,
		NESTED,
		RANGEFROM,
		RANGETO
}