package ftn.sf.reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Reaction {

	public enum ReactionType{
		UPVOTE, DOWNVOTE
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private ReactionType reactionType;
	private LocalDate timestamp;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "reacted_post", referencedColumnName = "id", nullable = true)
	private Post post;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "reacted_comment", referencedColumnName = "id", nullable = true)
	private Comment comment;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "reacted_by", referencedColumnName = "id", nullable = false)
	private User reactedBy;

	public Reaction(ReactionType reactionType, LocalDate timestamp, Post post, Comment comment, User reactedBy) {
		super();
		this.reactionType = reactionType;
		this.timestamp = timestamp;
		this.post = post;
		this.comment = comment;
		this.reactedBy = reactedBy;
	}

	public Reaction(ReactionType reactionType, LocalDate timestamp, Post post, User reactedBy) {
		this.reactionType = reactionType;
		this.timestamp = timestamp;
		this.post = post;
		this.reactedBy = reactedBy;
    }

	public Reaction(ReactionType reactionType, LocalDate timestamp, Comment comment, User reactedBy) {
		this.reactionType = reactionType;
		this.timestamp = timestamp;
		this.comment = comment;
		this.reactedBy = reactedBy;
    }
	
}
