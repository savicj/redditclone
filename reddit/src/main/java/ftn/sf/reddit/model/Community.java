package ftn.sf.reddit.model;

import lombok.*;

import static javax.persistence.FetchType.LAZY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Community {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String description;
	private boolean isSuspended;
	private String suspendedReason;
	private String pathToFile;
	
	@ManyToMany
	@JoinTable(
			name="communities_members",
			joinColumns = @JoinColumn(name="community_id"),
			inverseJoinColumns = @JoinColumn(name="user_id"))
	private Set<User> members = new HashSet<User>();
	
	@ManyToMany
	@JoinTable(
			name="communities_moderators",
			joinColumns = @JoinColumn(name="community_id"),
			inverseJoinColumns = @JoinColumn(name="moderator_id", referencedColumnName = "id"))
	private Set<Moderator> moderatedBy = new HashSet<Moderator>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = LAZY, mappedBy = "community")
    private Set<Post> posts = new HashSet<Post>();
	
	@ManyToMany
	@JoinTable(
			name="communities_flairs",
			joinColumns = @JoinColumn(name="community_id"),
			inverseJoinColumns = @JoinColumn(name="flair_id"))
	private Set<Flair> flairs = new HashSet<Flair>();
	
	@OneToMany(mappedBy = "bannedFrom")
	private Set<Banned> banned = new HashSet<Banned>();


	public Community( String name, String description,
					  boolean isSuspended, String suspendedReason, String pathToFile,
					Set<User> members, Set<Moderator> moderatedBy, Set<Post> posts, Set<Flair> flairs, Set<Banned> banned) {
		super();
		this.name = name;
		this.description = description;
		this.isSuspended = isSuspended;
		this.suspendedReason = suspendedReason;
		this.pathToFile = pathToFile;
		this.members = members;
		this.moderatedBy = moderatedBy;
		this.posts = posts;
		this.flairs = flairs;
		this.banned = banned;
	}

}
