package ftn.sf.reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Banned {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private LocalDate timestamp;
	@OneToOne
	@JoinColumn(name = "banned_by_moderator_id", referencedColumnName = "id", nullable = true)
	private Moderator by;
	@ManyToOne
	@JoinColumn(name = "banned_from", referencedColumnName = "id", nullable = true)
	private Community bannedFrom;
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = true)
	private User user;

}
