package ftn.sf.reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.FetchType.LAZY;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Comment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String text;
	private LocalDate timestamp;
	private boolean isDeleted;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = LAZY, mappedBy = "repliedTo")
	private Set<Comment> replies = new HashSet<Comment>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "replied_comment_id", referencedColumnName = "id", nullable = true)	
	private Comment repliedTo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "author", referencedColumnName = "id", nullable = true)
	private User author;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "commented_post", referencedColumnName = "id", nullable = false)
	private Post post;

	public Comment(String text, LocalDate timestamp, boolean isDeleted, Set<Comment> replies,
			Comment repliedTo, User author, Post post) {
		super();
		this.text = text;
		this.timestamp = timestamp;
		this.isDeleted = isDeleted;
		this.replies = replies;
		this.repliedTo = repliedTo;
		this.author = author;
		this.post = post;
	}

}
