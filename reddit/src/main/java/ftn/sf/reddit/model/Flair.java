package ftn.sf.reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Flair {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String name;
	
	@ManyToMany(mappedBy = "flairs")
	private Set<Community> community = new HashSet<Community>();

	public Flair(String name, Set<Community> community) {
		super();
		this.name = name;
		this.community = community;
	}

}
