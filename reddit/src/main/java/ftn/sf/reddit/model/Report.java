package ftn.sf.reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Report {

	public enum ReportReason{
		BREAKES_RULES,
		HARASSMENT,
		HATE,
		SHARING_PERSONAL_INFORMATION,
		IMPERSONATION,
		COPYRIGHT_VIOLATION,
		TRADEMARK_VIOLATION,
		SPAM,
		SELF_HARM_OR_SUICIDE,
		OTHER
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private ReportReason reason;
	private LocalDate timestamp;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name= "by_user", referencedColumnName = "id", nullable = false)
	private User byUser;
	private boolean accepted;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "reported_comment", referencedColumnName = "id", nullable = true)
	private Comment comment;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "reported_post", referencedColumnName = "id", nullable = true)
	private Post post;

	public Report(ReportReason reason, LocalDate timestamp, User byUser, boolean accepted, Comment comment, Post post) {
		super();
		this.reason = reason;
		this.timestamp = timestamp;
		this.byUser = byUser;
		this.accepted = accepted;
		this.comment = comment;
		this.post = post;
	}
}
