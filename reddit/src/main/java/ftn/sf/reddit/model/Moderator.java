package ftn.sf.reddit.model;

import lombok.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("MODERATOR")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@ToString
public class Moderator extends User{

	@ManyToMany(mappedBy = "moderatedBy")
    private Set<Community> moderates = new HashSet<>();

	public Moderator(Long id, String username, String password, String email, String avatar, LocalDate registrationDate,
			String description, String displayName, Set<Community> community, Set<Community> moderates) {
		super(id, username, password, email, avatar, registrationDate, description, displayName, community);
		this.moderates = moderates;
	}

	public Moderator(Long id, String username, String password, String email, String avatar, LocalDate registrationDate,
			String description, String displayName, Set<Community> community) {
		super(id, username, password, email, avatar, registrationDate, description, displayName, community);
	}
	public Moderator(String username, String password, String email, String avatar, LocalDate registrationDate,
            String description, String displayName, Set<Community> community, Set<Community> moderates2) {
				super(username, password, email, avatar, registrationDate, description, displayName, community);
		this.moderates = moderates2;
    }
}
