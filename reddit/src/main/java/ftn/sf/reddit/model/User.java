package ftn.sf.reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;


@Entity(name="users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="userRole", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("USER")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String username;
	private String password;
	private String email;
	private String avatar;
	private LocalDate registrationDate;
	private String description;
	private String displayName;
	@ManyToMany(mappedBy = "members")
	private Set<Community> community = new HashSet<Community>();

	public User(String username, String password, String email, String avatar, LocalDate registrationDate,
	String description, String displayName, Set<Community> community) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.avatar = avatar;
		this.registrationDate = registrationDate;
		this.description = description;
		this.displayName = displayName;
		this.community = community;
    }

}
