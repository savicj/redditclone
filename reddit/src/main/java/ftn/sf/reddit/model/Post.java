package ftn.sf.reddit.model;

import lombok.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Post {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String title;
	private String text;
	private LocalDate creationDate;
	private String imagePath;
	private String pathToFile;
	
	@ManyToOne
	@JoinColumn(name = "author", referencedColumnName = "id", nullable = false)
	private User author;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "flair", referencedColumnName = "id", nullable = true)
	private Flair flair;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "community_id", referencedColumnName = "id", nullable = true)
	private Community community;
	
	@OneToMany(mappedBy = "post")
	private Set<Comment> comments = new HashSet<Comment>();


	public Post(String title, String text, LocalDate creationDate,
				String imagePath, String pathToFile, User author, Flair flair,
			Community community, Set<Comment> comments) {
		super();
		this.title = title;
		this.text = text;
		this.creationDate = creationDate;
		this.imagePath = imagePath;
		this.pathToFile = pathToFile;
		this.author = author;
		this.flair = flair;
		this.community = community;
		this.comments = comments;
	}

}
