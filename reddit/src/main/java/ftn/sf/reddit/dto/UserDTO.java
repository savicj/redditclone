package ftn.sf.reddit.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDTO {

	private Long id;
	private String username;
	private String password;
	private String email;
	private String avatar;
	private LocalDate registrationDate;
	private String description;
	private String displayName;
	private Set<CommunityDTO> community = new HashSet<>();
	
	public UserDTO(String username, String password, String email, String avatar, LocalDate registrationDate,
			String description, String displayName,  Set<CommunityDTO> community) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.avatar = avatar;
		this.registrationDate = registrationDate;
		this.description = description;
		this.displayName = displayName;
		this.community = community;

	}
	public UserDTO(Long id,String username, String password, String email, String avatar, LocalDate registrationDate,
			String description, String displayName) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.avatar = avatar;
		this.registrationDate = registrationDate;
		this.description = description;
		this.displayName = displayName;
	}

	public UserDTO(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public UserDTO(User user) {
		super();
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.avatar = user.getAvatar();
		this.registrationDate = user.getRegistrationDate();
		this.description = user.getDescription();
		this.displayName = user.getDisplayName();
		Set<Community> communities = user.getCommunity();
		Set<CommunityDTO> dtos = new HashSet<>();
		for(Community c : communities){
			dtos.add(new CommunityDTO(c));
		}
		this.community = dtos;
	}

}
