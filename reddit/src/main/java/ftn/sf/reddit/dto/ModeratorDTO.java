package ftn.sf.reddit.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Moderator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ModeratorDTO extends UserDTO{

	private Set<CommunityDTO> moderates = new HashSet<>();

	public ModeratorDTO(Long id, String username, String password, String email, String avatar,
			LocalDate registrationDate, String description, String displayName, Set<CommunityDTO> community, Set<CommunityDTO> moderates) {
		super(id, username, password, email, avatar, registrationDate, description, displayName, community);
		this.moderates = moderates;
	}

	public ModeratorDTO(Moderator m) {
		super(m.getId(), m.getUsername(), m.getPassword(), m.getEmail(), m.getAvatar(), m.getRegistrationDate(), m.getDescription(), m.getDisplayName());
		Set<CommunityDTO> communitydto = new HashSet<>();
		for(Community c : m.getCommunity()) {
			CommunityDTO cdto = new CommunityDTO(c);
			communitydto.add(cdto);
		}
		this.setCommunity(communitydto);
		Set<CommunityDTO> moderatesdto = new HashSet<>();
		for(Community c : m.getModerates()) {
			CommunityDTO cdto = new CommunityDTO(c);
			moderatesdto.add(cdto);
		}
		this.moderates = moderatesdto;
	}
}
