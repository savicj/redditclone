package ftn.sf.reddit.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateCommunityDTO {

    private String name;
    private String description;
    private MultipartFile file;
    private FlairDTO flair;
}
