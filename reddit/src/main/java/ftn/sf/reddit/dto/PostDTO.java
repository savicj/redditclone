package ftn.sf.reddit.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import ftn.sf.reddit.lucene.elasticModel.PostES;
import ftn.sf.reddit.model.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PostDTO {

	private Long id;
	private String title;
	private String text;
	private UserDTO author;
	private LocalDate creationDate;
	private String imagePath;
	private FlairDTO flair;
	private CommunityDTO community;
	private Set<CommentDTO> comments = new HashSet<>();
	private String file;
	private int karma;

	public PostDTO(Post p) {
		super();
		this.id = p.getId();
		this.title = p.getTitle();
		this.text = p.getText();
		this.creationDate = p.getCreationDate();
		this.imagePath = p.getImagePath();
		if(p.getAuthor()!=null){
			this.author = new UserDTO(p.getAuthor());
		}else{
			this.author = null;
		}
		if(p.getFlair()==null){
			this.flair = null;
		}else{
			this.flair = new FlairDTO(p.getFlair());
		}
		if(p.getCommunity()==null){
			this.community = null;
		}else{
			this.community = new CommunityDTO(p.getCommunity());
		}

//		this.comments = comments;
	}


}
