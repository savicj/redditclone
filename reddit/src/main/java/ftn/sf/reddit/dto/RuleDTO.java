package ftn.sf.reddit.dto;

import ftn.sf.reddit.model.Rule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RuleDTO {

	private Long id;
	private String description;
	private CommunityDTO community;

	public RuleDTO(Rule r) {
		super();
		this.id = r.getId();
		this.description = r.getDescription();
		this.community = new CommunityDTO(r.getCommunity());
	}

}
