package ftn.sf.reddit.dto;

import java.time.LocalDate;

import ftn.sf.reddit.model.Reaction;
import ftn.sf.reddit.model.Reaction.ReactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReactionDTO {

	private Long id;
	private ReactionType reactionType;
	private LocalDate timestamp;
	private PostDTO post;
	private CommentDTO comment;
	private UserDTO reactedBy;

	public ReactionDTO(Reaction r) {
		super();
		this.id = r.getId();
		this.reactionType = r.getReactionType();
		this.timestamp = r.getTimestamp();
		this.post = new PostDTO(r.getPost());
		this.comment = new CommentDTO(r.getComment());
		this.reactedBy = new UserDTO(r.getReactedBy());
	}

	public ReactionDTO(ReactionType reactionType, LocalDate timestamp, PostDTO postDTO, UserDTO reactedBy) {
		this.reactionType = reactionType;
		this.timestamp = timestamp;
		this.post = postDTO;
		this.reactedBy = reactedBy;
	}
	public ReactionDTO(ReactionType reactionType, LocalDate timestamp, CommentDTO commentDTO, UserDTO reactedBy) {
		this.reactionType = reactionType;
		this.timestamp = timestamp;
		this.comment = commentDTO;
		this.reactedBy = reactedBy;
	}


}
