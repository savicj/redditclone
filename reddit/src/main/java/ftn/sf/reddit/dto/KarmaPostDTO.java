package ftn.sf.reddit.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class KarmaPostDTO {
    private PostDTO postDTO;
    private Long karma;
}
