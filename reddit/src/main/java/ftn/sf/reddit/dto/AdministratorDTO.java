package ftn.sf.reddit.dto;

import ftn.sf.reddit.model.Administrator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//@NoArgsConstructor
@AllArgsConstructor
@Data
public class AdministratorDTO extends UserDTO {

    public AdministratorDTO(Administrator a) {
        super(a.getId(), a.getUsername(), a.getPassword(), a.getEmail(), a.getAvatar(), a.getRegistrationDate(), 
            a.getDescription(), a.getDisplayName());
    }

}
