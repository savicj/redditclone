package ftn.sf.reddit.dto;

import java.time.LocalDate;

import ftn.sf.reddit.model.Report;
import ftn.sf.reddit.model.Report.ReportReason;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReportDTO {
	
	private Long id;
	private ReportReason reason;
	private LocalDate timestamp;
	private boolean accepted;
	private UserDTO byUser;
	private CommentDTO comment;
	private PostDTO post;

	public ReportDTO(Report r) {
		super();
		this.id = r.getId();
		this.reason = r.getReason();
		this.timestamp = r.getTimestamp();
		this.accepted = r.isAccepted();
		this.byUser = new UserDTO(r.getByUser());
		this.comment = new CommentDTO(r.getComment());
		this.post = new PostDTO(r.getPost());
	}

}
