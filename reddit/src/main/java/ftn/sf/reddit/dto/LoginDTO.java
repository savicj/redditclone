package ftn.sf.reddit.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoginDTO {
    
    private String username;
    private String password;
}
