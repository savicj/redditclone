package ftn.sf.reddit.dto;

import java.util.Collection;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoggedInUserDTO {
    
    private Long id;
    private UserDTO userDTO;
    private String token;
    private String username;
    private Collection<? extends GrantedAuthority> userDetails;


}
