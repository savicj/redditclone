package ftn.sf.reddit.dto;

import java.util.HashSet;
import java.util.Set;

import ftn.sf.reddit.model.Flair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlairDTO {

	private Long id;
	private String name;
	private Set<CommunityDTO> community = new HashSet<>();

	public FlairDTO(Flair f) {
		super();
		this.id = f.getId();
		this.name = f.getName();
//		this.community = community;
	}
	}
