package ftn.sf.reddit.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatePostDTO {

    private String title;
    private String text;
    private MultipartFile pdf;
    private String imagePath;
    private Long flairs;
    private Long community;
}
