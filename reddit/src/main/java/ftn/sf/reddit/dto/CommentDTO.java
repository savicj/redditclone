package ftn.sf.reddit.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import ftn.sf.reddit.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentDTO {

	private Long id;
	private String text;
	private LocalDate timestamp;
	private boolean isDeleted;
	private Set<CommentDTO> replies = new HashSet<>();
	private CommentDTO repliedTo;
	private UserDTO author;
	private PostDTO post;
	

	public CommentDTO(Comment c) {
		super();
		this.id = c.getId();
		this.text = c.getText();
		this.timestamp = c.getTimestamp();
		this.isDeleted = c.isDeleted();
//		this.replies = replies;
//		this.repliedTo = repliedTo;
		this.author = new UserDTO(c.getAuthor());
		this.post = new PostDTO(c.getPost());
	}

}
