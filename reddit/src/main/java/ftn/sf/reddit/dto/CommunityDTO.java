package ftn.sf.reddit.dto;

import java.util.HashSet;
import java.util.Set;

import ftn.sf.reddit.model.Banned;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Flair;
import ftn.sf.reddit.model.Moderator;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("unused")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CommunityDTO {

	private Long id;
	private String name;
	private String description;
	private boolean isSuspended;
	private String suspendedReason;
	private Set<UserDTO> members = new HashSet<UserDTO>();
	private Set<ModeratorDTO> moderatedBy = new HashSet<ModeratorDTO>();
	private Set<PostDTO> posts = new HashSet<PostDTO>();
	private Set<FlairDTO> flairs = new HashSet<FlairDTO>();
	private Set<BannedDTO> banned = new HashSet<BannedDTO>();
	private int karma;
	private String pdf;

	public CommunityDTO(Community c) {
		this.id = c.getId();
		this.name = c.getName();
		this.description = c.getDescription();
		this.isSuspended = c.isSuspended();
		this.suspendedReason = c.getSuspendedReason();
//		Set<UserDTO> mem = new HashSet<UserDTO>();
//		for(User u : c.getMembers()) {
//			mem.add(new UserDTO(u));
//		}
//		this.members = mem;
//		Set<ModeratorDTO> moderatorsdto = new HashSet<ModeratorDTO>();
//		for(Moderator m : c.getModeratedBy())
//			moderatorsdto.add(new ModeratorDTO(m));
//		this.moderatedBy = moderatorsdto;
//		Set<PostDTO> postsdto = new HashSet<PostDTO>();
//		for(Post p : c.getPosts())
//			postsdto.add(new PostDTO(p));
//		this.posts = postsdto;
//		Set<FlairDTO> flairsdto= new HashSet<FlairDTO>();
//		for(Flair p : c.getFlairs())
//			flairsdto.add(new FlairDTO(p));
//		this.flairs = flairsdto;
//		Set<BannedDTO> banneddto= new HashSet<BannedDTO>();
//		for(Banned p : c.getBanned())
//			banneddto.add(new BannedDTO(p));
//		this.banned = banneddto;
	}
}
