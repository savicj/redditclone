package ftn.sf.reddit.dto;

import java.time.LocalDate;

import ftn.sf.reddit.model.Banned;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BannedDTO {

	private Long id;
	private LocalDate timestamp;
	private ModeratorDTO by;
	private CommunityDTO bannedFrom;
	private UserDTO userDTO;

	public BannedDTO(Banned banned) {
		this.id = banned.getId();
		this.timestamp = banned.getTimestamp();
		this.by = new ModeratorDTO(banned.getBy());
		this.bannedFrom = new CommunityDTO(banned.getBannedFrom());
		this.userDTO = new UserDTO(banned.getUser());
	}
}
