package ftn.sf.reddit.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import ftn.sf.reddit.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ftn.sf.reddit.service.interfaces.CommunityServiceInterface;
import ftn.sf.reddit.service.interfaces.ModeratorServiceInterface;
import ftn.sf.reddit.service.interfaces.PostServiceInterface;
import lombok.extern.log4j.Log4j2;

@CrossOrigin("*")
@RestController
@RequestMapping("communities")
@Log4j2
public class CommunityController {
    
    @Autowired
    private ModeratorServiceInterface moderatorService;
    @Autowired
    private PostServiceInterface postService;
    @Autowired
    private CommunityServiceInterface communityService;

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping
    public ResponseEntity<List<CommunityDTO>> getAll(){
        log.info("get all communities");
        return new ResponseEntity<List<CommunityDTO>>(communityService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/unsuspended")
    public ResponseEntity<List<CommunityDTO>> getUnspended(){
        log.info("get not suspended communities");

        return new ResponseEntity<List<CommunityDTO>>(communityService.findByIsSuspended(false), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommunityDTO> getOne(@PathVariable Long id){
        log.info("get community with id {}", id);
        CommunityDTO c = communityService.findOne(id);
        return new ResponseEntity<CommunityDTO>(c, HttpStatus.OK);
    }


    @GetMapping("/boolean")
    public ResponseEntity<List<CommunityDTO>> findByMultipleFields(
            @RequestParam Optional<String> name,
            @RequestParam Optional<String> desc,
            @RequestParam Optional<String> pdf){
        log.info("get community with multiplefields ");
        if(name.isEmpty() && desc.isEmpty() && pdf.isEmpty())
            return new ResponseEntity<>(communityService.findByIsSuspended(false), HttpStatus.OK);
        List<SimpleQueryEs> simpleQueries = new ArrayList<>();
        if(name.isPresent()) simpleQueries.add(new SimpleQueryEs("name", name.get()));
        if(desc.isPresent()) simpleQueries.add(new SimpleQueryEs("description", desc.get()));
        if(pdf.isPresent()) simpleQueries.add(new SimpleQueryEs("pdfDescription", pdf.get()));

        return new ResponseEntity<>(communityService.findByMultipleFields(simpleQueries), HttpStatus.OK);
    }


    //NE RADI JOS
//    @GetMapping(value={"/noOfPosts/min/{min}/max/{max}", "/noOfPosts/min/{min}", "/noOfPosts/max/{max}"})
//    public ResponseEntity<List<CommunityDTO>> findByKarma(@PathVariable Optional<Integer> min,
//                                                          @PathVariable Optional<Integer> max){
//
//        log.info("get community with noOfPosts min {} and max {}", min, max);
//        List<CommunityDTO> communities = new ArrayList<>();
//        if(!min.isEmpty() && !max.isEmpty())
//            communities = communityService.findByNoOfPosts(min.get(), max.get());
//        if(!min.isEmpty() && max.isEmpty())
//            communities = communityService.findByNoOfPostsGreaterThan(min.get());
//        if(min.isEmpty() && !max.isEmpty())
//            communities = communityService.findByNoOfPostsLessThan(max.get());
//
//        return new ResponseEntity<>(communities, HttpStatus.OK);
//    }





    @GetMapping("/{id}/members")
    public ResponseEntity<List<UserDTO>> getMembers(@PathVariable Long id){
        log.info("FINDING MEMBERS!!");
        return new ResponseEntity<List<UserDTO>>(communityService.findMembers(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/moderators")
    public ResponseEntity<List<ModeratorDTO>> getModerators(@PathVariable Long id){
        log.info("finding moderators");
        return new ResponseEntity<List<ModeratorDTO>>(moderatorService.findByModerates(communityService.findOne(id)), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PostMapping("/{cId}/moderators/remove/{id}")
    public ResponseEntity<ModeratorDTO> removeModerator(@PathVariable Long cId, @PathVariable Long id){
        log.info("removing moderator");
        moderatorService.remove(id, cId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping(consumes = {"multipart/form-data"})
    public ResponseEntity<CommunityDTO> createCommunity(@ModelAttribute CreateCommunityDTO communityDTO) throws IOException {
        log.info("create community");
        communityService.save(communityDTO);

        return new ResponseEntity<>( HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('MODERATOR')")
    @PostMapping("/{id}/update")
    public ResponseEntity<CommunityDTO> updateCommunity(@PathVariable Long id, @RequestBody CommunityDTO communityDTO){
        CommunityDTO c = communityService.findOne(id);
        log.info("community update");
        if(c != null){
            CommunityDTO nova = communityDTO;
            nova.setId(id);
            nova.setMembers(c.getMembers());
            // Set<ModeratorDTO> moderators = c.getModeratedBy();
            // Set<UserDTO> usersModerators = new ArrayList();
            // for(ModeratorDTO m : moderators){
            //     usersModerators.add((UserDTO) m);
            // }
            nova.setModeratedBy(c.getModeratedBy());
            nova.setBanned(c.getBanned());
            nova.setPosts(c.getPosts());
            CommunityDTO dto = communityService.update(nova);
            return new ResponseEntity<CommunityDTO>(dto, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }
    }
    
    //azuriranje liste objava zajednice

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PostMapping("/{id}/suspend")
    public ResponseEntity<Void> suspendCommunity(@PathVariable Long id, @RequestBody String suspendedReason){
        communityService.suspend(id, suspendedReason);
        log.info("COMMUNITY {} SUSPENDED");
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping("/{id}/posts")
    public ResponseEntity<List<KarmaPostDTO>> getAllForCommunity(@PathVariable Long id){
        List<PostDTO> posts = postService.findByCommunity(communityService.findOne(id));
        List<KarmaPostDTO> karmaPostDTO = new ArrayList<>();
        for(PostDTO p : posts){
            Long karma = postService.calculateKarma(p.getId(), p.getId());
            karmaPostDTO.add(new KarmaPostDTO(p, karma));
        }
        // log.info("KARMAPOSTS OF A COMMUNITY {}", karmaPostDTO);
        return new ResponseEntity<List<KarmaPostDTO>>(karmaPostDTO, HttpStatus.OK);
    }








    // @PreAuthorize("hasRole('MODERATOR')")
    // @PutMapping("/{c}/{u}/ban")
    // public ResponseEntity<BannedDTO> ban(@PathVariable Long c, @PathVariable Long u){
    //     UserDTO user = userService.findById(u);
    //     if(user == null){
    //         return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    //     }
    //     BannedDTO bannedDTO = new BannedDTO(LocalDate.now(), communityService.findOne(c), , user);
    //     bannedService.save(bannedDTO);        
    //     return new ResponseEntity<BannedDTO>(HttpStatus.ACCEPTED);
    //  }
}
