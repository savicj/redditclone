package ftn.sf.reddit.controller;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.LoginDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.security.TokenUtils;
import ftn.sf.reddit.service.interfaces.CommunityServiceInterface;
import ftn.sf.reddit.service.interfaces.UserService;
import lombok.extern.log4j.Log4j2;

@CrossOrigin("*")
@RestController
@RequestMapping("users")
@Log4j2
public class UserController {
    
    @Autowired
    private UserService userService;   

    @Autowired
    private CommunityServiceInterface communityService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private PasswordEncoder passwordEncoder;


   
    @PostMapping()
    public ResponseEntity<UserDTO> create(@RequestBody @Validated UserDTO newUser){

        UserDTO createdUser = userService.createUser(newUser);

        if(createdUser == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDTO userDTO) {
        log.info("login fn "+userDTO);
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        System.err.println(authenticationToken.toString() + "\n" + authentication.toString());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(userDTO.getUsername());
            String token = tokenUtils.generateToken(userDetails);
            System.out.println(token);
            return ResponseEntity.ok(token);
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    
    @PutMapping(value ="/register", consumes = "application/json")
    public ResponseEntity<UserDTO> register(@RequestBody @Validated UserDTO userDTO){
            log.info("register");
            UserDTO user = userService.findByUsername(userDTO.getUsername());
            if(user!=null){
                return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
            }
    
            UserDTO noviKorisnik = new UserDTO();
            
            noviKorisnik.setUsername(userDTO.getUsername());
            noviKorisnik.setDisplayName(userDTO.getDisplayName());
            noviKorisnik.setAvatar(userDTO.getAvatar());
            noviKorisnik.setEmail(userDTO.getEmail());
            noviKorisnik.setRegistrationDate(LocalDate.now());
            noviKorisnik.setDescription(userDTO.getDescription());
            noviKorisnik.setCommunity(new HashSet<>());
            noviKorisnik.setPassword(passwordEncoder.encode(userDTO.getPassword()));

            userService.createUser(noviKorisnik);
    
    
            return new ResponseEntity<>(HttpStatus.CREATED);
    
        }
    
    
    //logout
    
    
    @PostMapping("/update")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO){
        userService.update(userDTO);
        return new ResponseEntity<UserDTO>(userDTO, HttpStatus.ACCEPTED);
    }   
    
    //change password untested
    @PostMapping("/updatePassword/{id}")
    public ResponseEntity<UserDTO> updatePassword(
        @PathVariable Long id,
        @RequestParam String oldpassword, 
        @RequestParam String newPassword
     ){
        UserDTO user = userService.findById(id);
        UserDTO novi = userService.updatePassword(user, oldpassword, newPassword);
        if(novi == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<UserDTO>(user, HttpStatus.ACCEPTED);
    } 


    @GetMapping("/{id}/karma")
    public ResponseEntity<Long> calculateKarma(@PathVariable Long id){
        Long karma = userService.calculateKarma(id, id);
        return new ResponseEntity<Long>(karma, HttpStatus.OK);
    }
    

    @GetMapping("/{username}")
    public ResponseEntity<UserDTO> findByUsername(@PathVariable String username){
        UserDTO user = userService.findByUsername(username);
        log.info(user);
         return new ResponseEntity<UserDTO>(user, HttpStatus.OK);
    }
   
    @GetMapping("/{username}/communities")
    public ResponseEntity<List<CommunityDTO>> findCommunitiesForUser(@PathVariable String username){
        UserDTO user = userService.findByUsername(username);
        List<CommunityDTO> communities = communityService.findByMembers(user);
        log.info("User is a member of next communities: {}", communities);
        return new ResponseEntity<List<CommunityDTO>>(communities, HttpStatus.OK);
    }

}
