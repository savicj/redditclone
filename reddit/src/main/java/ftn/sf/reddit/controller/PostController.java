package ftn.sf.reddit.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import ftn.sf.reddit.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ftn.sf.reddit.model.Reaction.ReactionType;
import ftn.sf.reddit.service.interfaces.CommunityServiceInterface;
import ftn.sf.reddit.service.interfaces.PostServiceInterface;
import ftn.sf.reddit.service.interfaces.ReactionServiceInterface;
import ftn.sf.reddit.service.interfaces.UserService;
import lombok.extern.log4j.Log4j2;

@CrossOrigin("*")
@RestController
@RequestMapping("posts")
@Log4j2
public class PostController {
    

    @Autowired
    private PostServiceInterface postService;
    @Autowired
    private UserService userService;
    @Autowired
    private ReactionServiceInterface reactionService;
    @Autowired
    private CommunityServiceInterface communityService;

    @GetMapping("/{id}")
    public ResponseEntity<PostDTO> getOne(@PathVariable Long id){
        log.info("get post with id {}", id);

        return new ResponseEntity<PostDTO>(postService.findOne(id), HttpStatus.OK);
    }

//    @GetMapping(value={"/karma/min/{min}/max/{max}", "/karma/min/{min}", "/karma/max/{max}"})
//    public ResponseEntity<List<KarmaPostDTO>> findByKarma(@PathVariable Optional<Integer> min,
//                                                          @PathVariable Optional<Integer> max){
//
//        log.info("get post with karma min {} and max {}", min, max);
//        List<PostDTO> posts = new ArrayList<>();
//        if(!min.isEmpty() && !max.isEmpty())
//            posts = postService.findByKarma(min.get(), max.get());
//        if(!min.isEmpty() && max.isEmpty())
//            posts = postService.findByKarmaGreaterThan(min.get());
//        if(min.isEmpty() && !max.isEmpty())
//            posts = postService.findByKarmaLessThan(max.get());
//        List<KarmaPostDTO> karmaPostDTO = new ArrayList<>();
//        for(PostDTO p : posts){
//            karmaPostDTO.add(new KarmaPostDTO(p, Long.parseLong(String.valueOf(p.getKarma()))));
//        }
//        return new ResponseEntity<>(karmaPostDTO, HttpStatus.OK);
//    }

    @GetMapping("/boolean")
    public ResponseEntity<List<KarmaPostDTO>> findByMultipleFields(
            @RequestParam Optional<String> title,
            @RequestParam Optional<String> text,
            @RequestParam Optional<String> pdf,
            @RequestParam Optional<Integer> min,
            @RequestParam Optional<Integer> max){
        log.info("get posts with multiplefields ");
        List<PostDTO> posts;
        if(title.isEmpty() && text.isEmpty() && pdf.isEmpty() && min.isEmpty() && max.isEmpty()){
            posts = postService.findAll();
        }else{
            List<SimpleQueryEs> simpleQueries = new ArrayList<>();
            if(title.isPresent()) simpleQueries.add(new SimpleQueryEs("title", title.get()));
            if(text.isPresent()) simpleQueries.add(new SimpleQueryEs("text", text.get()));
            if(pdf.isPresent()) simpleQueries.add(new SimpleQueryEs("pdfText", pdf.get()));
            if(min.isPresent() && max.isEmpty()) simpleQueries.add(new SimpleQueryEs("min", String.valueOf(min.get())));
            if(max.isPresent() && min.isEmpty()) simpleQueries.add(new SimpleQueryEs("max", String.valueOf(max.get())));
            String karmaRange ;
            if(max.isPresent() && min.isPresent()){
                karmaRange= min.get().toString() + "-" + max.get().toString();
                simpleQueries.add(new SimpleQueryEs("karma", karmaRange));
            }
            log.info(simpleQueries);
            posts = postService.findByMultipleFields(simpleQueries);
        }
        List<KarmaPostDTO> karmaPostDTO = new ArrayList<>();
        for(PostDTO p : posts){
            karmaPostDTO.add(new KarmaPostDTO(p, Long.parseLong(String.valueOf(p.getKarma()))));
        }
        return new ResponseEntity<>(karmaPostDTO, HttpStatus.OK);
    }
    
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KarmaPostDTO>> getAll(){
        log.info("get all posts");
        List<PostDTO> posts = postService.findAll();
        List<KarmaPostDTO> karmaPostDTO = new ArrayList<>();
        for(PostDTO p : posts){
//            Long karma = postService.calculateKarma(p.getId(), p.getId());
            Long karma = Long.parseLong(String.valueOf(p.getKarma()));
            karmaPostDTO.add(new KarmaPostDTO(p, karma));
//            log.info(karmaPostDTO);
        }
        return new ResponseEntity<List<KarmaPostDTO>>(karmaPostDTO, HttpStatus.OK);
    }
    //findallforcommunity in community controller jer je lepsi tj intuitivniji url 
    
    
    
    
    @GetMapping("/author/{authorId}")
    public ResponseEntity<List<PostDTO>> getAllforAuthor(@PathVariable Long authorId){
        return new ResponseEntity<List<PostDTO>>(postService.findByAuthor(userService.findById(authorId)), HttpStatus.OK);
    }
    
    
    
    
    @PutMapping(consumes = {"multipart/form-data"})
    public ResponseEntity<PostDTO> create(@ModelAttribute CreatePostDTO postDTO) throws IOException {
        log.info(postDTO);
        postService.save(postDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        log.info("deleting post");
        postService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    
    @PutMapping("/{id}/upvote")
    public ResponseEntity<KarmaPostDTO> upvote(@PathVariable Long id) throws IOException {
        log.info("upovitng post with id " + id);
        PostDTO postDTO = postService.findOne(id);
        if(postDTO != null){
            ReactionDTO reactionDTO = new ReactionDTO(); 
            reactionDTO.setReactionType(ReactionType.UPVOTE);
            reactionDTO.setTimestamp(LocalDate.now());
            reactionDTO.setPost(postDTO);
            reactionDTO.setComment(null);
            reactionDTO.setReactedBy(postDTO.getAuthor());
            reactionService.save(reactionDTO);
            postDTO.setKarma(postDTO.getKarma()+1);
            postService.upvote(postDTO);
            log.info("KARMA ZA ID"+ postDTO.getId() +" "+ postDTO.getKarma() );
            log.info("NOVA REAKCIJA == UPVOTE-OVANA OBJAVA, VRACAMO OBJAVU I KARMU");
            return new ResponseEntity<>( HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    
    
    
    
    @PutMapping("/{id}/downvote")
    public ResponseEntity<KarmaPostDTO> downvote(@PathVariable Long id) throws IOException {
        PostDTO postDTO = postService.findOne(id);
        if(postDTO != null){
            ReactionDTO reactionDTO = new ReactionDTO(); 
            reactionDTO.setReactionType(ReactionType.DOWNVOTE);
            reactionDTO.setTimestamp(LocalDate.now());
            reactionDTO.setPost(postDTO);
            reactionDTO.setComment(null);
            reactionDTO.setReactedBy(postDTO.getAuthor());
            reactionService.save(reactionDTO);
            postDTO.setKarma(postDTO.getKarma()-1);
            postService.downvote(postDTO);
            log.info("NOVA REAKCIJA == downVOTE-OVANA OBJAVA, VRACAMO OBJAVU, REAKCIJU I KARMU");
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    
    
    
    
    @GetMapping("/{id}/karma")
    public ResponseEntity<Long> getKarma(@PathVariable Long id){
        Long karma = postService.calculateKarma(id, id);
        
        return new ResponseEntity<Long>(karma, HttpStatus.OK);
    }
}
