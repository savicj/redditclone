package ftn.sf.reddit.service.interfaces;

import java.util.List;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.ModeratorDTO;
import ftn.sf.reddit.model.Moderator;

public interface ModeratorServiceInterface {
    
    public ModeratorDTO findOne(Long id);
    public List<ModeratorDTO> findAll();
    public List<ModeratorDTO> findByModerates(CommunityDTO communityDTO);
    public void save(ModeratorDTO flairDTO);
    public void update(ModeratorDTO flairDTO);
    public boolean remove(Long moderator, Long community);
    public Moderator populateModerator(ModeratorDTO moderatorDTO, Moderator moderator);
}
