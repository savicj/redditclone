package ftn.sf.reddit.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.ModeratorDTO;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Moderator;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.ModeratorRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.ModeratorServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class ModeratorService implements ModeratorServiceInterface {
    
    @Autowired
    private ModeratorRepository moderatorRepository;
    @Autowired
    private CommunityRespository communityRespository;
    @Autowired
    private UserRepository<User> userRepository;
    
    @Override
    public ModeratorDTO findOne(Long id) {
        log.info("find by id {}", id);
        return new ModeratorDTO(moderatorRepository.findById(id).orElse(null));
    }

    @Override
    public List<ModeratorDTO> findAll() {
        log.info("find all moderators");
        List<Moderator> moderators = moderatorRepository.findAll();
        List<ModeratorDTO> dtos = new ArrayList<>();
        for(Moderator m : moderators){
            dtos.add(new ModeratorDTO(m));
        }
        return dtos;
    }

    @Override
    public List<ModeratorDTO> findByModerates(CommunityDTO communityDTO) {
        log.info("find all moderators for community");
        List<Moderator> moderators = moderatorRepository.findByModerates(communityRespository.getById(communityDTO.getId()));
        List<ModeratorDTO> dtos = new ArrayList<>();
        for(Moderator m : moderators){
            dtos.add(new ModeratorDTO(m));
        }
        return dtos;
    }

    @Override
    public void save(ModeratorDTO moderatorDTO) {
        Moderator m = moderatorRepository.findById(moderatorDTO.getId()).orElse(null);
        if(m == null){
            moderatorRepository.save(populateModerator(moderatorDTO, new Moderator()));
            log.info("moderator saved");
        }
        
    }

    @Override
    public void update(ModeratorDTO moderatorDTO) {
        Moderator m = moderatorRepository.findById(moderatorDTO.getId()).orElse(null);
        if(m != null){
            moderatorRepository.save(populateModerator(moderatorDTO, new Moderator()));
            log.info("moderator saved");
        }
        
    }

    @Override
    public boolean remove(Long m, Long c) {
        log.info("removing comunity from moderator");
    
        Moderator moderator = moderatorRepository.getById(m);
        Community community = communityRespository.getById(c);
        moderator.getModerates().remove(community);
        community.getModeratedBy().remove(moderator);
        communityRespository.save(community);
        log.info("community updated");
        if(moderator.getModerates() == null || moderator.getModerates().isEmpty()){
            User user = new User(moderator.getUsername(), moderator.getPassword(),
                         moderator.getEmail(), moderator.getAvatar(), moderator.getRegistrationDate(),
                         moderator.getDescription(), moderator.getDisplayName(), moderator.getCommunity());
            moderatorRepository.delete(moderator);
            log.info("moderator deleted from database");
            userRepository.save(user);
            log.info("moderator saved as a new user");
            return true;
        }else if(moderator.getModerates() != null && !moderator.getModerates().isEmpty()){
            moderatorRepository.save(moderator);
            return false;
        }

        return false;
    }

    @Override
    public Moderator populateModerator(ModeratorDTO moderatorDTO, Moderator moderator) {
        moderator.setAvatar(moderatorDTO.getAvatar());
        List<Community> community = communityRespository.findByMembers(moderatorRepository.getById(moderatorDTO.getId())); 
        Set<Community> communities = new HashSet<>(community);
        moderator.setCommunity(communities);
        moderator.setDescription(moderatorDTO.getDescription());
        moderator.setDisplayName(moderatorDTO.getDisplayName());
        moderator.setEmail(moderatorDTO.getEmail());
        Set<CommunityDTO> moderatesdto = moderatorDTO.getModerates();
        Set<Community> moderates = new HashSet<>();
        for(CommunityDTO communityDTO : moderatesdto){
            Community c = communityRespository.getById(communityDTO.getId());
            moderates.add(c);
        }
        moderator.setModerates(moderates);
        moderator.setPassword(moderatorDTO.getPassword());
        moderator.setUsername(moderatorDTO.getUsername());
        moderator.setRegistrationDate(moderatorDTO.getRegistrationDate());
        

        return moderator;
    }
    
}
