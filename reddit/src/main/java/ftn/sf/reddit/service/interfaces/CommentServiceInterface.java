package ftn.sf.reddit.service.interfaces;

import java.util.List;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.dto.PostDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Comment;

public interface CommentServiceInterface {
    
    public CommentDTO findOne(Long id);
    public List<CommentDTO> findAll();
    public List<CommentDTO> findByRepliedTo(CommentDTO commentDTO);
    public List<CommentDTO> findByAuthor(UserDTO userDTO);
    public List<CommentDTO> findByPost(PostDTO post);
    public Long calculateKarma(Long id, Long id2);
    public void save(CommentDTO commentDTO);
    public void update(CommentDTO commentDTO);
    public void remove(Long id);
    public Comment populateComment(CommentDTO comment, Comment comment2);
}
