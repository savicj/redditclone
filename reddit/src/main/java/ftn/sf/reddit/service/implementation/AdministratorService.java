package ftn.sf.reddit.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.AdministratorDTO;
import ftn.sf.reddit.model.Administrator;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.repository.AdministratorRepository;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.service.interfaces.AdministratorServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class AdministratorService implements AdministratorServiceInterface {
    
    @Autowired
    private AdministratorRepository administratorRepository;
    @Autowired
    private CommunityRespository communityRespository;
    
    
    @Override
    public AdministratorDTO findOne(Long id) {
        log.info("find one");
        return new AdministratorDTO(administratorRepository.findById(id).orElse(null));
    }

    @Override
    public AdministratorDTO findByUsername(String username) {
        log.info("find by username");
        Administrator a = administratorRepository.findByUsername(username);
        return new AdministratorDTO(a);
    }

    @Override
    public List<AdministratorDTO> findAll() {
        List<Administrator> administrators = administratorRepository.findAll();
        List<AdministratorDTO> dtos = new ArrayList<>();
        for(Administrator a : administrators){
            dtos.add(new AdministratorDTO(a));
        }
        return dtos;
    }

    @Override
    public void save(AdministratorDTO administrator) {
        Administrator a = administratorRepository.findById(administrator.getId()).orElse(null);
        if(a == null){
            administratorRepository.save(populateAdministrator(administrator, new Administrator()));
        }
        
    }

    @Override
    public void update(AdministratorDTO administrator) {
        Administrator a = administratorRepository.findById(administrator.getId()).orElse(null);
        if(a != null){
            administratorRepository.save(populateAdministrator(administrator, a));
        }
    }

    @Override
    public void remove(Long id) {
        administratorRepository.removeAdministrator(id);
        
    }

    @Override
    public Administrator populateAdministrator(AdministratorDTO administratorDTO, Administrator administrator) {
        administrator.setAvatar(administratorDTO.getAvatar());
        List<Community> community = communityRespository.findByMembers(administratorRepository.getById(administratorDTO.getId())); 
        Set<Community> communities = new HashSet<>(community);
        administrator.setCommunity(communities);
        administrator.setDescription(administratorDTO.getDescription());
        administrator.setDisplayName(administratorDTO.getDisplayName());
        administrator.setEmail(administratorDTO.getEmail());
        administrator.setPassword(administratorDTO.getPassword());
        administrator.setUsername(administratorDTO.getUsername());
        administrator.setRegistrationDate(administratorDTO.getRegistrationDate());
        

        return administrator;
    }
    
}
