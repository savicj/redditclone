package ftn.sf.reddit.service.implementation;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

import javax.transaction.Transactional;

import ftn.sf.reddit.dto.*;
import ftn.sf.reddit.lucene.elasticModel.PostES;
import ftn.sf.reddit.lucene.esRepository.PostRepositoryES;
import ftn.sf.reddit.lucene.indexing.handlers.PDFHandler;
import ftn.sf.reddit.lucene.search.SearchType;
import ftn.sf.reddit.model.*;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.model.Reaction.ReactionType;
import ftn.sf.reddit.repository.CommentRepository;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.FlairRepository;
import ftn.sf.reddit.repository.PostRepository;
import ftn.sf.reddit.repository.ReactionRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.PostServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class PostService implements PostServiceInterface { 

    @Value("${files.path}")
    private String dataFilesPath;;
    @Value("${app.upload.dir:${user.home}}")
    private String uploadDir;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository<User> userRepository;
    @Autowired
    private FlairRepository flairRepository;
    @Autowired
    private CommunityRespository communityRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired 
    private ReactionRepository reactionRepository;

    @Autowired
    private PostRepositoryES postRepositoryES;
    
    @Override
    public PostDTO findOne(Long id) {
        log.info("find one with id {}", id);
        return populateDTOFromES(postRepositoryES.findById(id).orElse(null));
    }

    @Override
    public List<PostDTO> findAll() {
        log.info("find all posts");
        Iterable<PostES> posts = postRepositoryES.findAll();
        List<PostDTO> dtos = new ArrayList<>();
        for(PostES p : posts){
            dtos.add(populateDTOFromES(p));
        }
        return dtos;
    }



    @Override
    public List<PostDTO> findByMultipleFields(List<SimpleQueryEs> simpleQueries) {
        log.info("searching multiple fields");
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        for(SimpleQueryEs sq : simpleQueries){
            if(sq.getField().equals("min"))
                boolQueryBuilder.filter(SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("karma", sq.getValue()), SearchType.RANGEFROM));
            else if(sq.getField().equals("max"))
                boolQueryBuilder.filter(SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("karma", sq.getValue()), SearchType.RANGETO));
            else if(sq.getField().equals("karma"))
                boolQueryBuilder.filter(SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("karma", sq.getValue()), SearchType.RANGE));
            else
               boolQueryBuilder.must(SearchQueryGenerator.createMatchQueryBuilder(sq));
        }
        return getDTOsFromHits(searchByBoolQuery(boolQueryBuilder));
    }

    private SearchHits<PostES> searchByBoolQuery(BoolQueryBuilder boolQueryBuilder) {
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .build();

        return elasticsearchRestTemplate.search(searchQuery, PostES.class,  IndexCoordinates.of("posts"));
    }



    public List<PostDTO> getDTOsFromHits(SearchHits<PostES> hits){
        List<PostDTO> dtos = new ArrayList<>();
        for(SearchHit<PostES> hit : hits)
            dtos.add(populateDTOFromES(hit.getContent()));

        return dtos;
    }

    @Override
    public List<PostDTO> findByCreationDate(LocalDate localDate) {
        log.info("find all posts for date");
        List<Post> posts = postRepository.findByCreationDate(localDate);
        List<PostDTO> dtos = new ArrayList<>();
        for(Post p : posts){
            dtos.add(new PostDTO(p));
        }
        return dtos;
    }

    @Override
    public List<PostDTO> findByAuthor(UserDTO author) {
        log.info("find all posts by user");
        List<Post> posts = postRepository.findByAuthor(userRepository.getById(author.getId()));
        List<PostDTO> dtos = new ArrayList<>();
        for(Post p : posts){
            dtos.add(new PostDTO(p));
        }
        return dtos;
    }

    @Override
    public List<PostDTO> findByFlair(FlairDTO flairDTO) {
        log.info("find all posts with flair");
        List<Post> posts = postRepository.findByFlair(flairRepository.getById(flairDTO.getId()));
        List<PostDTO> dtos = new ArrayList<>();
        for(Post p : posts){
            dtos.add(new PostDTO(p));
        }
        return dtos;
    }

    @Override
    public List<PostDTO> findByCommunity(CommunityDTO communityDTO) {
        log.info("find all posts of a community");
        List<Post> posts = postRepository.findByCommunity(communityRepository.getById(communityDTO.getId()));
        List<PostDTO> dtos = new ArrayList<>();
        for(Post p : posts){
            dtos.add(new PostDTO(p));
        }
        return dtos;
    }

    @Override
    public void save(CreatePostDTO postDTO) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User userDTO = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
        User author = userRepository.findByUsername(userDTO.getUsername());

        Community community = communityRepository.getById(postDTO.getCommunity());

        Flair flair ;
        if(postDTO.getFlairs() != null) flair = flairRepository.getById(postDTO.getFlairs());
        else flair = null;

        String filename = "";
        if(postDTO.getPdf() != null) filename = postDTO.getPdf().getOriginalFilename();

        Post savedentity = saveEntity(postDTO, author, community, flair, filename);
        indexWithUploadedFile(postDTO, author.getId(), savedentity.getId());

        log.info("post document and entity saved!!!!!!!!!!");


    }

    public Post saveEntity(CreatePostDTO postDTO, User author, Community community, Flair flair, String filename){
        Post post = Post.builder()
                .title(postDTO.getTitle())
                .text(postDTO.getText())
                .community(community)
                .author(author)
                .creationDate(LocalDate.now())
                .flair(flair)
                .pathToFile(filename)
                .imagePath(postDTO.getImagePath())
                .build();

        Post savedentity = postRepository.save(post);
        community.getPosts().add(post);
        communityRepository.save(community);
        log.info("community saved");

        Reaction reaction = new Reaction(ReactionType.UPVOTE, LocalDate.now(), post, post.getAuthor());
        reactionRepository.save(reaction);
        log.info("post upvoted! ");
        return savedentity;
    }

    public void indexWithUploadedFile(CreatePostDTO postDTO, Long author, Long id) throws IOException {
        PostES postES = new PostES();
        String fileName = saveUploadedFileInFolder(postDTO.getPdf());
        if(fileName != null){
            PDFHandler pdfHandler = new PDFHandler();
            postES = pdfHandler.getPostEs(new File(fileName));
        }
        postES.setId(id);
        postES.setTitle(postDTO.getTitle());
        postES.setText(postDTO.getText());
        postES.setAuthor(author);
        postES.setCreationDate(LocalDate.now());
        postES.setImagePath(postDTO.getImagePath());
        postES.setFlair(postDTO.getFlairs());
        postES.setCommunity(postDTO.getCommunity());
        postES.setComments(new ArrayList<>());
        postES.setKarma(1);
        log.info(postES);
        postRepositoryES.save(postES);
    }


    @Override
    public void update(PostDTO postDTO) throws IOException {
        Post post = postRepository.findById(postDTO.getId()).orElse(null);
        if(post != null){
            PostES postES = postRepositoryES.findById(postDTO.getId()).get();
            postRepositoryES.save(populatePostES(postDTO, postES));
            postRepository.save(populatePost(postDTO, post));
            log.info("new post saved ");
        }
    }

    @Override
    public void remove(Long id) {
        postRepository.deleteById(id);
        log.info("post deleted by id {}", id);

    }

    @Override
    public Long calculateKarma(Long id, Long id2) {
        Long karma = postRepository.calculateKarma(id, id2);
        if(karma == null)
            karma = (long)0;
        return karma;
    }
    public void upvote(PostDTO postDTO) throws IOException {
        Post post = postRepository.findById(postDTO.getId()).orElse(null);
        PostES postES = postRepositoryES.findById(postDTO.getId()).get();
        postES.setKarma(postDTO.getKarma());
        postRepositoryES.save(postES);
        postRepository.save(populatePost(postDTO, post));
        log.info("upvoted");
    }
    public void downvote(PostDTO postDTO) throws IOException {
        Post post = postRepository.findById(postDTO.getId()).orElse(null);
        PostES postES = postRepositoryES.findById(postDTO.getId()).get();
        postES.setKarma(postDTO.getKarma());
        postRepositoryES.save(postES);
        postRepository.save(populatePost(postDTO, post));
        log.info("downvoted");
    }

    public List<PostDTO> mapDTOS (SearchHits<PostES> posts){
        return posts.map(post -> populateDTOFromES(post.getContent())).toList();
    }
    public Post populatePost(PostDTO postDTO, Post post) {
        post.setAuthor(userRepository.findById(postDTO.getAuthor().getId()).orElse(null));
        if(postDTO.getCreationDate() == null){
            post.setCreationDate(LocalDate.now());
        }else{
            post.setCreationDate(postDTO.getCreationDate());
        }
        post.setImagePath(postDTO.getImagePath());
        post.setText(postDTO.getText());
        post.setTitle(postDTO.getTitle());
        if(postDTO.getFlair() != null){
            post.setFlair(flairRepository.findById(postDTO.getFlair().getId()).orElse(null));
        }else{
            post.setFlair(null);
        }
        post.setCommunity(communityRepository.findById(postDTO.getCommunity().getId()).orElse(null));
        Set<CommentDTO> dtos = postDTO.getComments();
        Set<Comment> comments = new HashSet<>();
        for(CommentDTO c : dtos){
            comments.add(commentRepository.getById(c.getId()));
        }
        post.setComments(comments);
//        post.setPathToFile(postDTO.getFile());

        return post;
    }
    public PostDTO populateDTOFromES(PostES es){
        if(es.equals(null))
            return null;
        UserDTO author = new UserDTO(userRepository.getById(es.getAuthor()));
        FlairDTO flair = null;
        if(es.getFlair() != null)
            flair = new FlairDTO(flairRepository.getById(es.getFlair()));
        CommunityDTO community = new CommunityDTO(communityRepository.getById(es.getCommunity()));
        Set<CommentDTO> comments = new HashSet<>();
        for(Long c : es.getComments())
            comments.add(new CommentDTO(commentRepository.getById(c)));
        PostDTO dto = PostDTO.builder()
                .id(es.getId())
                .title(es.getTitle())
                .text(es.getText())
                .author(author)
                .creationDate(es.getCreationDate())
                .imagePath(es.getImagePath())
                .flair(flair)
                .community(community)
                .comments(comments)
                .file(es.getPdfText())
                .karma(es.getKarma())
                .build();

        return dto;
    }
    public PostES populatePostES(PostDTO postDTO, PostES post) throws IOException {
        post.setTitle(postDTO.getTitle());
        post.setText(postDTO.getText());
        if(postDTO.getCreationDate() == null){
            post.setCreationDate(LocalDate.now());
        }else{
            post.setCreationDate(postDTO.getCreationDate());
        }
        post.setKarma(post.getKarma());
        return post;
    }



    public File getResourceFilePath(String path) {
        URL url = this.getClass().getClassLoader().getResource(path);
        File file = null;
        try {
            if(url != null) {
                file = new File(url.toURI());
            }
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        return file;
    }

    public String saveUploadedFileInFolder(MultipartFile file) throws IOException {
        String retVal = null;
        if (file !=null && !file.isEmpty()) {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(getResourceFilePath(dataFilesPath).getAbsolutePath() + File.separator + file.getOriginalFilename());
            Files.write(path, bytes);
            Path filepath = Paths.get(uploadDir + File.separator + StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())));
            Files.write(filepath, bytes);
            retVal = path.toString();
        }
        return retVal;
    }



}
