package ftn.sf.reddit.service.implementation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.dto.PostDTO;
import ftn.sf.reddit.dto.ReactionDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Reaction;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.model.Reaction.ReactionType;
import ftn.sf.reddit.repository.CommentRepository;
import ftn.sf.reddit.repository.PostRepository;
import ftn.sf.reddit.repository.ReactionRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.ReactionServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class ReactionService implements ReactionServiceInterface {
    
    @Autowired
    private ReactionRepository reactionRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository<User> userRepository;

    @Override
    public ReactionDTO findOne(Long id) {
        log.info("finding one reaction by id {}", id);
        return new ReactionDTO(reactionRepository.findById(id).orElse(null));
    }

    @Override
    public List<ReactionDTO> findAll() {
        log.info("finding all reactions");
        List<Reaction> reactions = reactionRepository.findAll(); 
        List<ReactionDTO> dtos = new ArrayList<>();
        for(Reaction r : reactions){
            dtos.add(new ReactionDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReactionDTO> findByReactionType(ReactionType reactionType) {
        log.info("finding all reactions by reaction type {}", reactionType);
        List<Reaction> reactions = reactionRepository.findByReactionType(reactionType);
        List<ReactionDTO> dtos = new ArrayList<>();
        for(Reaction r : reactions){
            dtos.add(new ReactionDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReactionDTO> findByTimestamp(LocalDate timestamp) {
        log.info("finding all reactions by timestamp {}", timestamp);
        List<Reaction> reactions = reactionRepository.findByTimestamp(timestamp);
        List<ReactionDTO> dtos = new ArrayList<>();
        for(Reaction r : reactions){
            dtos.add(new ReactionDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReactionDTO> findByPost(PostDTO postDTO) {
        log.info("finding all reactions for post {}", postDTO.getTitle());
        List<Reaction> reactions = reactionRepository.findByPost(postRepository.getById(postDTO.getId()));
        List<ReactionDTO> dtos = new ArrayList<>();
        for(Reaction r : reactions){
            dtos.add(new ReactionDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReactionDTO> findByComment(CommentDTO commentDTO) {
        log.info("finding all reactions for comment {}", commentDTO.getText());
        List<Reaction> reactions = reactionRepository.findByComment(commentRepository.getById(commentDTO.getId()));
        List<ReactionDTO> dtos = new ArrayList<>();
        for(Reaction r : reactions){
            dtos.add(new ReactionDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReactionDTO> findByReactedBy(UserDTO reactedBy) {
        log.info("finding all reactions by user {}", reactedBy.getUsername());
        List<Reaction> reactions = reactionRepository.findByReactedBy(userRepository.getById(reactedBy.getId()));
        List<ReactionDTO> dtos = new ArrayList<>();
        for(Reaction r : reactions){
            dtos.add(new ReactionDTO(r));
        }
        return dtos;
    }

    @Override
    public void save(ReactionDTO reactionDTO) {
        log.info(reactionDTO.toString());
        if(reactionDTO.getId() == null){
            Reaction reaction = populateReaction(reactionDTO, new Reaction());
            reactionRepository.save(reaction);
            log.info("reaction saved");
        }
        
    }

    @Override
    public void update(ReactionDTO reactionDTO) {
        Reaction reaction = reactionRepository.findById(reactionDTO.getId()).orElse(null);
        if(reaction != null){
            reactionRepository.save(reaction);
            log.info("reaction {} updated", reactionDTO.getId());

        }
    }

    @Override
    public void remove(Long id) {
        reactionRepository.deleteById(id); 
        log.info("reaction {} deleted", id);       
    }

    @Override
    public Reaction populateReaction(ReactionDTO reactionDTO, Reaction reaction) {
        reaction.setReactionType(reactionDTO.getReactionType());
        reaction.setTimestamp(reactionDTO.getTimestamp());
        if(reactionDTO.getPost() == null){
            reaction.setPost(null);
        }else{
            reaction.setPost(postRepository.getById(reactionDTO.getPost().getId()));
        }
        if(reactionDTO.getComment() == null){
            reaction.setComment(null);
        }else{
            reaction.setComment(commentRepository.getById(reactionDTO.getComment().getId()));
        }
        reaction.setReactedBy(userRepository.getById(reactionDTO.getReactedBy().getId()));

        return reaction;
    }
}
