package ftn.sf.reddit.service.implementation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.BannedDTO;
import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.ModeratorDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Banned;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.repository.BannedRepository;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.ModeratorRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.BannedServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class BannedService implements BannedServiceInterface {
    
    @Autowired
    private BannedRepository bannedRepository;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserRepository<User> userRepository;
    @Autowired
    private CommunityRespository communityRespository;
    @Autowired
    private ModeratorRepository moderatorRepository;

    @Override
    public BannedDTO findOne(Long id) {
        log.info("Find one by id: {}", id);
        return new BannedDTO(bannedRepository.findById(id).orElse(null));
    }

    @Override
    public List<BannedDTO> findAll() {
        log.info("Find all banned");
        List<Banned> banned = bannedRepository.findAll();
        List<BannedDTO> dtos = new ArrayList<>();
        for(Banned b : banned){
            dtos.add(new BannedDTO(b));
        }
        return dtos;
    }

    @Override
    public BannedDTO findByUser(UserDTO userDTO) {
        log.info("Find banned user");
        User user = userService.populateUser(userDTO, new User());
        user.setId(userDTO.getId());
        Banned banned = bannedRepository.findByUser(user);
        return new BannedDTO(banned);
    }

    @Override
    public List<BannedDTO> findByBannedFrom(CommunityDTO communityDTO) {
        log.info("Find all banned from community: {}", communityDTO.getName());
        List<Banned> banned = bannedRepository.findByBannedFrom(communityRespository.getById(communityDTO.getId()));
        List<BannedDTO> dtos = new ArrayList<>();
        for(Banned b : banned){
            dtos.add(new BannedDTO(b));
        }
        return dtos;
    }

    @Override
    public List<BannedDTO> findByBy(ModeratorDTO moderatorDTO) {
        log.info("Find all banned by moderator {}", moderatorDTO.getUsername());
        List<Banned> banned = bannedRepository.findByBy(moderatorRepository.getById(moderatorDTO.getId()));
        List<BannedDTO> dtos = new ArrayList<>();
        for(Banned b : banned){
            dtos.add(new BannedDTO(b));
        }
        return dtos;
        }

    @Override
    public List<BannedDTO> findByTimestamp(LocalDate timestamp) {
        log.info("Find all banned by timestamp {}", timestamp);
        List<Banned> banned = bannedRepository.findByTimestamp(timestamp);
        List<BannedDTO> dtos = new ArrayList<>();
        for(Banned b : banned){
            dtos.add(new BannedDTO(b));
        }
        return dtos;
    }

    @Override
    public void save(BannedDTO bannedDTO) {
        log.info("Save new banned");
        Banned banned = bannedRepository.findById(bannedDTO.getId()).orElse(null);
        if(banned != null){
            log.info("Error: Banned already exists!");
        }
        Banned b = populateBanned(bannedDTO, new Banned());
        b.setTimestamp(LocalDate.now());
        bannedRepository.save(b);
        log.info("Saved");
    }

    @Override
    public void update(BannedDTO bannedDTO) {
        log.info("Update banned with id: {}", bannedDTO.getId());
        Banned banned = bannedRepository.findById(bannedDTO.getId()).orElse(null);
        if(banned != null){
            Banned b = populateBanned(bannedDTO, banned);
            bannedRepository.save(b);
            log.info("updated");
        }
        
    }

    @Override
    public void remove(Long id) {
        log.info("Deleted banned by id: {}", id);
        bannedRepository.deleteById(id);
        
    }

    @Override
    public Banned populateBanned(BannedDTO bannedDTO, Banned banned) {
        banned.setBannedFrom(communityRespository.findById(bannedDTO.getBannedFrom().getId()).orElse(null));
        banned.setBy(moderatorRepository.findById(bannedDTO.getBy().getId()).orElse(null));
        banned.setTimestamp(bannedDTO.getTimestamp());
        banned.setUser(userRepository.findById(bannedDTO.getUserDTO().getId()).orElse(null));

        return banned;
    }    
    
}
