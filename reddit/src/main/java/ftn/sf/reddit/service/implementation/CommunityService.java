package ftn.sf.reddit.service.implementation;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

import javax.transaction.Transactional;

import ftn.sf.reddit.dto.*;
import ftn.sf.reddit.lucene.elasticModel.CommunityES;
import ftn.sf.reddit.lucene.elasticModel.PostES;
import ftn.sf.reddit.lucene.esRepository.CommunityRepositoryES;
import ftn.sf.reddit.lucene.esRepository.PostRepositoryES;
import ftn.sf.reddit.lucene.indexing.handlers.PDFHandler;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.model.Banned;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Flair;
import ftn.sf.reddit.model.Moderator;
import ftn.sf.reddit.model.Post;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.repository.BannedRepository;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.FlairRepository;
import ftn.sf.reddit.repository.ModeratorRepository;
import ftn.sf.reddit.repository.PostRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.CommunityServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class CommunityService implements CommunityServiceInterface {

    @Value("${files.path}")
    private String dataFilesPath;;
    @Value("${app.upload.dir:${user.home}}")
    private String uploadDir;


    @Autowired
    private CommunityRespository communityRespository;
    @Autowired
    private CommunityRepositoryES communityRespositoryEs;
    @Autowired
    private ModeratorRepository moderatorRepository;
    @Autowired
    private ModeratorService moderatorService;
    @Autowired
    private BannedRepository bannedRepository;
    @Autowired
    private FlairRepository flairRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private PostRepositoryES postRepositoryEs;
    @Autowired
    private PostService postService;
    @Autowired
    private UserRepository<User> userRepository;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;


    @Override
    public CommunityDTO findOne(Long id) {
        log.info("Find community by id: {}", id);
        Community c = communityRespository.findById(id).orElse(null);
        if(c != null){
            log.info(c.toString());
            CommunityDTO communityDTO = populateDTO(c);
            return communityDTO;
        }
        return null;
    }

    @Override
    public List<CommunityDTO> findAll() {
        log.info("Finding all communities");
        List<Community> communities = communityRespository.findAll();
        List<CommunityDTO> dtos = new ArrayList<>();
        for(Community c : communities){
            dtos.add(populateDTO(c));
        }return dtos;
    }


    @Override
    public List<CommunityDTO> findByNoOfPosts(int min, int max){ return null; }
    @Override
    public List<CommunityDTO> findByNoOfPostsGreaterThan(int min) { return null; }
    @Override
    public List<CommunityDTO> findByNoOfPostsLessThan(int max) { return null; }

    @Override
    public List<CommunityDTO> findByMultipleFields(List<SimpleQueryEs> simpleQueries){
        log.info("searching multiple fields");
        log.info(simpleQueries);
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        for(SimpleQueryEs sq : simpleQueries)
            boolQueryBuilder.must(SearchQueryGenerator.createMatchQueryBuilder(sq));

        List<CommunityDTO> dtos = new ArrayList<>();
        for(SearchHit<CommunityES> hit : searchByBoolQuery(boolQueryBuilder))
            dtos.add(populateDTOFromES(hit.getContent()));

        return dtos;
    }

    private SearchHits<CommunityES> searchByBoolQuery(BoolQueryBuilder boolQueryBuilder) {
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .build();

        return elasticsearchRestTemplate.search(searchQuery, CommunityES.class,  IndexCoordinates.of("communities"));
    }

    private QueryBuilder createNestedQueryBuilder(SimpleQueryEs simpleQueryEs, String fieldName, ScoreMode scoreMode) {
        BoolQueryBuilder boolQueryBuilder = boolQuery();
        BoolQueryBuilder nestedBoolQueryBuilder =
                boolQuery().must(
                        boolQuery()
                                .should(termQuery(fieldName, simpleQueryEs.getValue())));
        QueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery(simpleQueryEs.getField(), nestedBoolQueryBuilder, scoreMode);
        return boolQueryBuilder.must(nestedQueryBuilder);
    }


    @Override
    public List<CommunityDTO> findByIsSuspended(boolean isSuspended) {
        log.info("Find community by suspension: {}", isSuspended);
        List<CommunityES> communities = communityRespositoryEs.findByIsSuspended(isSuspended);
        List<CommunityDTO> dtos = new ArrayList<>();
        for(CommunityES c : communities){
            dtos.add(populateDTOFromES(c));
        }
        log.info(dtos);
        return dtos;
    }

    @Override
    public List<CommunityDTO> findByModeratedBy(ModeratorDTO moderatorDTO) {
        log.info("Find communities by moderator: {}", moderatorDTO.getDisplayName());
       List<Community> communities = communityRespository.findByModeratedBy(moderatorRepository.getById(moderatorDTO.getId()));
        List<CommunityDTO> dtos = new ArrayList<>();
        for(Community c : communities){
            dtos.add(new CommunityDTO(c));
        }
        return dtos;
    }

    @Override
    public CommunityDTO findByPost(PostDTO postDTO) {
        log.info("Find community by post: {}", postDTO.getTitle());
       CommunityDTO dtos = new CommunityDTO( communityRespository.findByPosts(postRepository.getById(postDTO.getId())));
        
        return dtos;
    }

    @Override
    public List<CommunityDTO> findByMembers(UserDTO userDTO) {
        log.info("Find communities by member: {}", userDTO.getDisplayName());
        List<Community> communities = communityRespository.findByMembers(userRepository.getById(userDTO.getId()));
        List<CommunityDTO> dtos = new ArrayList<>();
        for(Community c : communities){
            dtos.add(new CommunityDTO(c));
        }
        return dtos;
    }

    @Override
    public List<UserDTO> findMembers(Long id) {
        Community c = communityRespository.findById(id).orElse(null);
        if(c != null){
            List<String> usernames = userRepository.findMembers(c.getId());
            List<User> members = new ArrayList<>();
            for(String u : usernames){
                members.add(userRepository.findByUsername(u));
            }
            List<UserDTO> dtos = new ArrayList<>();
            for(User u : members){
                dtos.add(new UserDTO(u));
            }
            return dtos;
        }
        return null;
    }

    @Override
    public List<CommunityDTO> findByFlairs(FlairDTO flairDTO) {
        log.info("Find communities by flair: {}", flairDTO.getName());
        List<Community> communities = communityRespository.findByFlairs(flairRepository.getById(flairDTO.getId()));
        List<CommunityDTO> dtos = new ArrayList<>();
        for(Community c : communities){
            dtos.add(new CommunityDTO(c));
        }
        return dtos;
    }

    @Override
    public List<CommunityDTO> findByBanned(BannedDTO bannedDTO) {
        log.info("Find communities by banned user: {}", bannedDTO.getUserDTO().getUsername());
        List<Community> communities = communityRespository.findByBanned(bannedRepository.getById(bannedDTO.getId()));
        List<CommunityDTO> dtos = new ArrayList<>();
        for(Community c : communities){
            dtos.add(new CommunityDTO(c));
        }
        return dtos;
    }



    @Override
    public void save(CreateCommunityDTO communityDTO) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User userDTO = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
        User user = userRepository.findByUsername(userDTO.getUsername());

        Community community = saveEntity(communityDTO, user);
        indexWithUploadedFile(communityDTO, user.getId(), community.getId());
    }

    private Community saveEntity(CreateCommunityDTO communityDTO, User user){
        Set<User> members = new HashSet<>();
        members.add(user);
        Set<Moderator> moderators = new HashSet<>();
        Moderator m;
        if(user.getClass().isInstance(Moderator.class)){
            moderators.add((Moderator) user);
        }else{
            m = new Moderator(user.getId(), user.getUsername(), user.getPassword(), user.getEmail(), user.getAvatar(), user.getRegistrationDate(),
                    user.getDescription(), user.getDisplayName(), new HashSet<>());
        }
        Set<Post> posts = new HashSet<>();
        Set<Flair> flairs = new HashSet<>();
        Set<Banned> banned = new HashSet<>();

        String filename = "";
        if(communityDTO.getFile() != null) filename = communityDTO.getFile().getOriginalFilename();

        Community community = Community.builder()
                .name(communityDTO.getName())
                .description(communityDTO.getDescription())
                .isSuspended(false)
                .pathToFile(filename)
                .members(members)
                .moderatedBy(moderators)
                .posts(posts)
                .flairs(flairs)
                .banned(banned)
                .build();

        Community entity = communityRespository.save(community);
        log.info("entity saved!");

        if(!user.getClass().isInstance(Moderator.class)){
            m = new Moderator(user.getId(), user.getUsername(), user.getPassword(), user.getEmail(), user.getAvatar(), user.getRegistrationDate(),
                    user.getDescription(), user.getDisplayName(), new HashSet<>());
            Set<Community> moderates = new HashSet<>();
            moderates.add(communityRespository.getById(entity.getId()));
            m.setModerates(moderates);
            moderatorRepository.save(m);
//            userRepository.deleteById(user.getId());
            log.info("user was not a moderator; moderator saved");
        }else{
            ((Moderator) user).getModerates().add(communityRespository.getById(entity.getId()));
            moderatorRepository.save((Moderator) user);
            log.info("user was a moderator; moderates updated");
        }

        return entity;
    }

    private void indexWithUploadedFile(CreateCommunityDTO communityDTO, Long moderator, Long id) throws IOException {
        String filename = postService.saveUploadedFileInFolder(communityDTO.getFile());
        CommunityES communityES = new CommunityES();
        if(filename != null){
            PDFHandler pdfHandler = new PDFHandler();
            communityES = pdfHandler.getCommunityEs(new File(filename));
        }
        communityES.setId(id);
        communityES.setName(communityDTO.getName());
        communityES.setDescription(communityDTO.getDescription());
        communityES.setSuspended(false);

        communityRespositoryEs.save(communityES);
        log.info("communityES saved!");
    }

    @Override
    public CommunityDTO update(CommunityDTO communityDTO) {
        Community community = communityRespository.findById(communityDTO.getId()).orElse(null);
        if(community != null){
            community.setName(communityDTO.getName());
            community.setDescription(communityDTO.getDescription());
            communityRespository.save(community);
        }
        
        return populateDTO(community);
    }   

    @Override
    public void suspend(Long id, String suspendedReason) {
        log.info("Suspension of a community; community is removed from moderator");
        Community community = communityRespository.findById(id).orElse(null);
        if(community != null){
            community.setSuspended(true);
            community.setSuspendedReason(suspendedReason);
            communityRespository.save(community);
            List<Moderator> dtos = moderatorRepository.findByModerates(community);
            for(Moderator m : dtos){
                moderatorService.remove(m.getId(), community.getId());
                log.info("community removed from moderator");
            }
        }
    }


    private CommunityDTO populateDTO(Community c) {
        CommunityDTO communityDTO = new CommunityDTO(c);
        Set<UserDTO> mem = new HashSet<UserDTO>();
        for(User u : c.getMembers()) {
            if(u!=null){
                mem.add(new UserDTO(u));
            }
        }
        communityDTO.setMembers(mem);
        Set<ModeratorDTO> moderatorsdto = new HashSet<ModeratorDTO>();
        for(Moderator m : c.getModeratedBy()){
            if(m!=null){
                moderatorsdto.add(new ModeratorDTO(m));
            }
        }
        communityDTO.setModeratedBy(moderatorsdto);
        Set<PostDTO> postsdto = new HashSet<PostDTO>();
        for(Post p : c.getPosts()){
            if(p!=null){
                postsdto.add(new PostDTO(p));
            }
        }
        communityDTO.setPosts(postsdto);
        Set<FlairDTO> flairsdto= new HashSet<FlairDTO>();
        for(Flair p : c.getFlairs()){
            if(p!=null){
                flairsdto.add(new FlairDTO(p));
            }
        }
        communityDTO.setFlairs(flairsdto);
        Set<BannedDTO> banneddto= new HashSet<BannedDTO>();
        for(Banned p : c.getBanned()){
            if(p!=null){
                banneddto.add(new BannedDTO(p));
            }
        }
        communityDTO.setBanned(banneddto);
        return communityDTO;
    }

    private Community populateCommunity(CommunityDTO dto, Community c){
        c.setName(dto.getName());
        c.setDescription(dto.getDescription());
        c.setSuspended(dto.isSuspended());
        c.setSuspendedReason(dto.getSuspendedReason());
        Set<UserDTO> membersDTO = dto.getMembers();
        Set<User> members = new HashSet<>();
        for(UserDTO userDTO : membersDTO){
           members.add(userRepository.getById(userDTO.getId()));   
        }
        c.setMembers(members);

        Set<ModeratorDTO> moderatedByDTO = dto.getModeratedBy();
        Set<Moderator> moderatedBy = new HashSet<>();
        for(ModeratorDTO moderatorDTO : moderatedByDTO){ 
            Long id = moderatorDTO.getId();       
            log.info("MODERATOR ID {}!!!!!\n\n\n", id );  
            moderatedBy.add(moderatorRepository.getById(id));   
        }
        c.setModeratedBy(moderatedBy);

        Set<PostDTO> postsDTO = dto.getPosts();
        Set<Post> posts = new HashSet<>();
        for(PostDTO postDTO : postsDTO){
            posts.add(postRepository.getById(postDTO.getId()));   
        }
        c.setPosts(posts);
        
        Set<FlairDTO> flairsDTO = dto.getFlairs();
        Set<Flair> flairs = new HashSet<>();
        for(FlairDTO flairDTO : flairsDTO){
            flairs.add(flairRepository.getById(flairDTO.getId()));   
        }
        c.setFlairs(flairs);
        
        Set<BannedDTO> bannedDTOs = dto.getBanned();
        Set<Banned> banned = new HashSet<>();
        for(BannedDTO bannedDTO : bannedDTOs){
            banned.add(bannedRepository.getById(bannedDTO.getId()));   
        }
        c.setBanned(banned);

        return c;
    }

    private CommunityDTO populateDTOFromES(CommunityES es){
        Set<FlairDTO> flairs = new HashSet<>();
        if(es.getFlairs() != null){
            for(Long f : es.getFlairs())
                flairs.add(new FlairDTO(flairRepository.getById(f)));
        }

        Set<UserDTO> members = new HashSet<>();
        if(es.getMembers() != null){
            for(Long f : es.getMembers())
                members.add(new UserDTO(userRepository.getById(f)));
        }

        Set<ModeratorDTO> moderators = new HashSet<>();
        if(es.getModeratedBy() != null){
            for(Long f : es.getModeratedBy())
                moderators.add(new ModeratorDTO(moderatorRepository.getById(f)));
        }

        Set<PostDTO> posts = new HashSet<>();
        if(es.getPosts() != null){
            for(PostES f : es.getPosts())
                posts.add(new PostDTO(postRepository.getById(f.getId())));
        }

        Set<BannedDTO> banned = new HashSet<>();
        if(es.getBanned() != null){
            for(Long f : es.getBanned())
                banned.add(new BannedDTO(bannedRepository.getById(f)));
        }
        CommunityDTO dto = CommunityDTO.builder()
                .id(es.getId())
                .name(es.getName())
                .description(es.getDescription())
                .isSuspended(es.isSuspended())
                .suspendedReason(es.getSuspendedReason())
                .members(members)
                .moderatedBy(moderators)
                .posts(posts)
                .flairs(flairs)
                .banned(banned)
                .karma(calculateKarma(communityRespository.getById(es.getId())))
                .pdf(es.getPdfDescription())
                .build();

        return dto;
    }

    private List<CommunityDTO> getDTOsFromESs(List<CommunityES> es){
        List<CommunityDTO> dtos = new ArrayList<>();
        for(CommunityES e : es){
            dtos.add(populateDTOFromES(e));
        }
        return dtos;
    }

    private int calculateKarma(Community community){
        List<Post> posts = postRepository.findByCommunity(community);
        int totalKarma = 0;
        for(Post p : posts){
            totalKarma += postRepository.calculateKarma(p.getId(), p.getId());
        }
        if(totalKarma == 0) return 0;
        if(posts.isEmpty()) return 0;

        return totalKarma/ posts.size();
    }



//    private File getResourceFilePath(String path) {
//        URL url = this.getClass().getClassLoader().getResource(path);
//        File file = null;
//        try {
//            if(url != null) {
//                file = new File(url.toURI());
//            }
//        } catch (URISyntaxException e) {
//            file = new File(url.getPath());
//        }
//        return file;
//    }
//    private String saveUploadedFileInFolder(MultipartFile file) throws IOException {
//        String retVal = null;
//        if (file !=null && !file.isEmpty()) {
//            byte[] bytes = file.getBytes();
//            Path path = Paths.get(getResourceFilePath(dataFilesPath).getAbsolutePath() + File.separator + file.getOriginalFilename());
//            Files.write(path, bytes);
//            Path filepath = Paths.get(uploadDir + File.separator + StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())));
//            Files.write(filepath, bytes);
//            retVal = path.toString();
//        }
//        return retVal;
//    }
}
