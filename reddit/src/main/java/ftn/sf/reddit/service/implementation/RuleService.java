package ftn.sf.reddit.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.RuleDTO;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Rule;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.RuleRepository;
import ftn.sf.reddit.service.interfaces.RuleServiceInterface;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class RuleService implements RuleServiceInterface {
    
    @Autowired
    private RuleRepository ruleRepository;
    @Autowired
    private CommunityRespository communityRespository;
    
    @Override
    public RuleDTO findOne(Long id) {
        return new RuleDTO(ruleRepository.findById(id).orElse(null));
    }

    @Override
    public List<RuleDTO> findAll() {
        List<Rule> rules  = ruleRepository.findAll();
        List<RuleDTO> dtos = new ArrayList<>();
        for(Rule r : rules){
            dtos.add(new RuleDTO(r));
        }
        return dtos;
    }

    @Override
    public List<RuleDTO> findByDescription(String description) {
        List<Rule> rules  = ruleRepository.findByDescription(description);
        List<RuleDTO> dtos = new ArrayList<>();
        for(Rule r : rules){
            dtos.add(new RuleDTO(r));
        }
        return dtos;
    }

    @Override
    public List<RuleDTO> findByCommunity(CommunityDTO communityDTO) {
        List<Rule> rules  = ruleRepository.findByCommunity(communityRespository.getById(communityDTO.getId()));
        List<RuleDTO> dtos = new ArrayList<>();
        for(Rule r : rules){
            dtos.add(new RuleDTO(r));
        }
        return dtos;
    }

    @Override
    public void save(RuleDTO ruleDTO) {
        Rule r = new Rule();
        r.setId(ruleDTO.getId());
        r.setDescription(ruleDTO.getDescription());
        Community community = new Community();

        r.setCommunity(community);
        
    }

    @Override
    public void update(RuleDTO ruleDTO) {
        
        
    }

    @Override
    public void remove(Long id) {
        ruleRepository.deleteById(id);
    }

    @Override
    public Rule populateCommunity(RuleDTO ruleDTO, Rule rule) {
        rule.setDescription(ruleDTO.getDescription());
        rule.setCommunity(communityRespository.findById(ruleDTO.getCommunity().getId()).orElse(null));
        
        return rule;
    }
    
    
}
