package ftn.sf.reddit.service.interfaces;

import java.util.List;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.FlairDTO;
import ftn.sf.reddit.model.Flair;

public interface FlairServiceInterface {
    
    public FlairDTO findOne(Long id);
    public List<FlairDTO> findAll();
    public FlairDTO findByName(String name);
    public List<FlairDTO> findByCommunity(CommunityDTO communityDTO);
    public void save(FlairDTO flairDTO);
    public void update(FlairDTO flairDTO);
    public void remove(Long id);
    public Flair populateFlair(FlairDTO flairDTO, Flair flair);
}
