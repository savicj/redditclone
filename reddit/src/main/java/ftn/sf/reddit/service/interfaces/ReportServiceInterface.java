package ftn.sf.reddit.service.interfaces;

import java.time.LocalDate;
import java.util.List;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.dto.PostDTO;
import ftn.sf.reddit.dto.ReportDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Report;
import ftn.sf.reddit.model.Report.ReportReason;

public interface ReportServiceInterface {
    
    public ReportDTO findOne(Long id);
    public List<ReportDTO> findAll();
    public List<ReportDTO> findByReportReason(ReportReason reason);
    public List<ReportDTO> findByTimestamp(LocalDate timestamp);
    public List<ReportDTO> findByByUser(UserDTO byUser);
    public List<ReportDTO> findByAccepted(boolean accepted);
    public List<ReportDTO> findByComment(CommentDTO commentDTO);
    public List<ReportDTO> findByPost(PostDTO postDTO);
    public void save(ReportDTO reportDTO);
    public void update(ReportDTO reportDTO);
    public void remove(Long id);
    public Report populateReport(ReportDTO reportDTO, Report report);
}
