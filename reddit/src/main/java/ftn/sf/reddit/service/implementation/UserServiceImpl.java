package ftn.sf.reddit.service.implementation;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository<User> userRepository;
    @Autowired
    private CommunityRespository communityRespository;
    

    @Override
    public UserDTO findByUsername(String username) {
        Optional<User> user = userRepository.findFirstByUsername(username);
        if (!user.isEmpty()) {
            return new UserDTO(user.get());
        }
        return null;
    }

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        Optional<User> user = userRepository.findFirstByUsername(userDTO.getUsername());
        if(user.isPresent()){
            return null;
        }
        User newUser = populateUser(userDTO, new User());
        newUser.setRegistrationDate(LocalDate.now());
        userRepository.save(newUser);
        return new UserDTO(newUser);
    }

    @Override
    public String getRole(Long id) {
        return userRepository.getRole(id);
    }

    @Override
    public UserDTO update(UserDTO userDTO){
        User u = userRepository.findById(userDTO.getId()).orElse(null);
        if(u != null){
            u = populateUser(userDTO, u);
            userRepository.save(u);
            log.info("user updated");
        }
        return new UserDTO(u);
    }

    @Override
    public User populateUser(UserDTO userDTO, User user) {
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setAvatar(userDTO.getAvatar());
        user.setRegistrationDate(userDTO.getRegistrationDate());
        user.setDescription(userDTO.getDescription());
        user.setDisplayName(userDTO.getDisplayName());
        Set<CommunityDTO> dtos = userDTO.getCommunity();
        Set<Community> communities = new HashSet<>();
        for(CommunityDTO c: dtos){
            communities.add(communityRespository.getById(c.getId()));
        }
        user.setCommunity(communities);
        return user;
    }

    @Override
    public UserDTO findById(Long id) {
        return new UserDTO(userRepository.findById(id).orElse(null));
    }

    @Override
    public UserDTO updatePassword(UserDTO userDTO, String oldpassword, String newPassword) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User userDetails = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
        String currentpassword = userDetails.getPassword();
        User u = userRepository.getById(userDTO.getId());
        if(!currentpassword.equals(oldpassword)){
            return null;
        }
        userDTO.setPassword(newPassword);
        userRepository.save(populateUser(userDTO, u));
        
        return userDTO;
    }

    @Override
    public Long calculateKarma(Long id, Long id2) {
        return userRepository.calculateKarma(id, id2);
    }
    
}
