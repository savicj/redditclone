package ftn.sf.reddit.service.interfaces;

import java.io.IOException;
import java.util.List;

import ftn.sf.reddit.dto.*;
import ftn.sf.reddit.model.Community;

public interface CommunityServiceInterface {
    
    CommunityDTO findOne(Long id);

    List<CommunityDTO> findAll();

    List<CommunityDTO> findByNoOfPosts(int min, int max);
    List<CommunityDTO> findByNoOfPostsGreaterThan(int min);
    List<CommunityDTO> findByNoOfPostsLessThan(int max);

    List<CommunityDTO> findByMultipleFields(List<SimpleQueryEs> es);

    List<CommunityDTO> findByIsSuspended(boolean isSuspended);
    List<CommunityDTO> findByModeratedBy(ModeratorDTO moderatorDTO);
    CommunityDTO findByPost(PostDTO postDTO);
    List<CommunityDTO> findByMembers(UserDTO userDTO);
    List<UserDTO> findMembers(Long id);
    List<CommunityDTO> findByFlairs(FlairDTO flairDTO);
    List<CommunityDTO> findByBanned(BannedDTO bannedDTO);

    void save(CreateCommunityDTO commentDTO) throws IOException;
    CommunityDTO update(CommunityDTO commentDTO);

    void suspend(Long id, String suspendedReason);

}
/*
za sedmicu mi fali opseg broja objava
BooleanQuery kombinacija parametara ocena 8
opis pravila ocena 9
opseg karme zajednice ocena 10
*/