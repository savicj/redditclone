package ftn.sf.reddit.service.interfaces;

import java.util.List;

import ftn.sf.reddit.dto.AdministratorDTO;
import ftn.sf.reddit.model.Administrator;

public interface AdministratorServiceInterface {
    
    public AdministratorDTO findOne(Long id);
    public AdministratorDTO findByUsername(String username);
    public List<AdministratorDTO> findAll();
    public void save(AdministratorDTO administrator);
    public void update(AdministratorDTO administrator);
    public void remove(Long id);
    public Administrator populateAdministrator(AdministratorDTO administratorDTO, Administrator administrator);
}
