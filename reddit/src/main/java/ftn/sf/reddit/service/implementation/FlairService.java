package ftn.sf.reddit.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.FlairDTO;
import ftn.sf.reddit.model.Community;
import ftn.sf.reddit.model.Flair;
import ftn.sf.reddit.repository.CommunityRespository;
import ftn.sf.reddit.repository.FlairRepository;
import ftn.sf.reddit.service.interfaces.FlairServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class FlairService implements FlairServiceInterface {
    
    @Autowired
    private FlairRepository flairRepository;
    @Autowired
    private CommunityRespository communityRespository;
    
    @Override
    public FlairDTO findOne(Long id) {
        log.info("find flair with id {}", id);
        return new FlairDTO(flairRepository.getById(id));
    }

    @Override
    public List<FlairDTO> findAll() {
        log.info("find all flairs");
        List<Flair> flairs = flairRepository.findAll();
        List<FlairDTO> dtos = new ArrayList<>(); 
        for(Flair f : flairs){
            dtos.add(new FlairDTO(f));
        }
        return dtos;
    }

    @Override
    public FlairDTO findByName(String name) {
        log.info("find  flair with name {}" ,name);
        return new FlairDTO(flairRepository.findByName(name));
    }

    @Override
    public List<FlairDTO> findByCommunity(CommunityDTO communityDTO) {
        log.info("find all flairs for community {}", communityDTO.getId());
        List<Flair> flairs = flairRepository.findByCommunity(communityRespository.getById(communityDTO.getId()));
        List<FlairDTO> dtos = new ArrayList<>(); 
        for(Flair f : flairs){
            dtos.add(new FlairDTO(f));
        }
        return dtos;
    }

    @Override
    public void save(FlairDTO flairDTO) {
        Flair flair = flairRepository.findById(flairDTO.getId()).orElse(null);
        if(flair == null){
            log.info("flair saved");
            flairRepository.save(populateFlair(flairDTO, new Flair()));
        }else{
            log.info("Error: flair with id {} already exists!", flairDTO.getId());
        }
        
    }

    @Override
    public void update(FlairDTO flairDTO) {
        Flair flair = flairRepository.findById(flairDTO.getId()).orElse(null);
        if(flair != null){
            log.info("updating flair");
            flairRepository.save(populateFlair(flairDTO, flair));
        }else{
            log.info("there is no flair with said id");
        }
        
    }

    @Override
    public void remove(Long id) {
        flairRepository.deleteById(id);
        
    }

    @Override
    public Flair populateFlair(FlairDTO flairDTO, Flair flair) {
        flair.setName(flairDTO.getName());
        flair.setCommunity(new HashSet<Community>());
        // Set<CommunityDTO> dtos = flairDTO.getCommunity();
        // for(CommunityDTO c : dtos){
        //     flair.getCommunity().add(communityRespository.getById(c.getId()));
        // }
        return flair;
    }
    
}
