package ftn.sf.reddit.service.interfaces;

import java.time.LocalDate;
import java.util.List;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.dto.PostDTO;
import ftn.sf.reddit.dto.ReactionDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Reaction;
import ftn.sf.reddit.model.Reaction.ReactionType;

public interface ReactionServiceInterface {
    
    public ReactionDTO findOne(Long id);
    public List<ReactionDTO> findAll();
    public List<ReactionDTO> findByReactionType(ReactionType reactionType);
    public List<ReactionDTO> findByTimestamp(LocalDate timestamp);
    public List<ReactionDTO> findByPost(PostDTO post);
    public List<ReactionDTO> findByComment(CommentDTO commentDTO);
    public List<ReactionDTO> findByReactedBy(UserDTO reactedBy);
    public void save(ReactionDTO reactionDTO);
    public void update(ReactionDTO reactionDTO);
    public void remove(Long id);
    public Reaction populateReaction(ReactionDTO reactionDTO, Reaction reaction);
}
