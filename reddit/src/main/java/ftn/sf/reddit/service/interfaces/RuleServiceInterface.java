package ftn.sf.reddit.service.interfaces;

import java.util.List;

import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.RuleDTO;
import ftn.sf.reddit.model.Rule;

public interface RuleServiceInterface {
   
    public RuleDTO findOne(Long id);
    public List<RuleDTO> findAll();
    public List<RuleDTO> findByDescription(String description);
    public List<RuleDTO> findByCommunity(CommunityDTO communityDTO);
    public void save(RuleDTO ruleDTO);
    public void update(RuleDTO ruleDTO);
    public void remove(Long id);
    public Rule populateCommunity(RuleDTO ruleDTO, Rule rule);
}
