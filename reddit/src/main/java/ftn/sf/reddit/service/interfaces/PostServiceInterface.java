package ftn.sf.reddit.service.interfaces;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import ftn.sf.reddit.dto.*;
import ftn.sf.reddit.model.Post;

public interface PostServiceInterface {
    
    PostDTO findOne(Long id);

    List<PostDTO> findAll();

    List<PostDTO> findByMultipleFields(List<SimpleQueryEs> es);

    List<PostDTO> findByCreationDate(LocalDate localDate);
    List<PostDTO> findByAuthor(UserDTO author);
    List<PostDTO> findByFlair(FlairDTO flairDTO);
    List<PostDTO> findByCommunity(CommunityDTO communityDTO);

    Long calculateKarma(Long id, Long id2);

    void save(CreatePostDTO postDTO) throws IOException;
    void update(PostDTO postDTO) throws IOException;
    void remove(Long id);

    void upvote(PostDTO postDTO) throws IOException;
    void downvote(PostDTO postDTO) throws IOException;

}
