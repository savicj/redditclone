package ftn.sf.reddit.service.implementation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.dto.PostDTO;
import ftn.sf.reddit.dto.ReportDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Report;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.model.Report.ReportReason;
import ftn.sf.reddit.repository.CommentRepository;
import ftn.sf.reddit.repository.PostRepository;
import ftn.sf.reddit.repository.ReportRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.ReportServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class ReportService implements ReportServiceInterface{
    
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private UserRepository<User> userRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostRepository postRepository;
    
    @Override
    public ReportDTO findOne(Long id) {
        log.info("find by id : {}", id);
        return new ReportDTO(reportRepository.findById(id).orElse(null));
    }

    @Override
    public List<ReportDTO> findAll() {
        log.info("find all");
        List<Report> reports = reportRepository.findAll();
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReportDTO> findByReportReason(ReportReason reason) {
        log.info("find by report reason : {}", reason);
        List<Report> reports = reportRepository.findByReason(reason);
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReportDTO> findByTimestamp(LocalDate timestamp) {
        log.info("find by timestamp : {}", timestamp);
        List<Report> reports = reportRepository.findByTimestamp(timestamp);
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReportDTO> findByByUser(UserDTO byUser) {
        log.info("find by by user : {}", byUser.getUsername());
        List<Report> reports = reportRepository.findByByUser(userRepository.findById(byUser.getId()).orElse(null));
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReportDTO> findByAccepted(boolean accepted) {
        log.info("find by accepted : {}", accepted);
        List<Report> reports = reportRepository.findByAccepted(accepted);
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReportDTO> findByComment(CommentDTO commentDTO) {
        log.info("find by comment : {}", commentDTO.getText());
        List<Report> reports = reportRepository.findByComment(commentRepository.getById(commentDTO.getId()));
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public List<ReportDTO> findByPost(PostDTO postDTO) {
        log.info("find by post : {}", postDTO.getTitle());
        List<Report> reports = reportRepository.findByPost(postRepository.getById(postDTO.getId()));
        List<ReportDTO> dtos = new ArrayList<>();
        for(Report r : reports){
            dtos.add(new ReportDTO(r));
        }
        return dtos;
    }

    @Override
    public void save(ReportDTO reportDTO) {
        reportRepository.save(populateReport(reportDTO, new Report()));
        log.info("saved report");
    }

    @Override
    public void update(ReportDTO reportDTO) {
        Report report = reportRepository.findById(reportDTO.getId()).orElse(null);
        if(report != null){
            log.info("updated report with id : {}", reportDTO.getId());
            reportRepository.save(populateReport(reportDTO, report));
        }
        
    }

    @Override
    public void remove(Long id) {
        reportRepository.deleteById(id);
        log.info("delete by id : {}", id);
        
    }

    @Override
    public Report populateReport(ReportDTO reportDTO, Report report) {
        report.setAccepted(reportDTO.isAccepted());
        report.setReason(reportDTO.getReason());
        report.setTimestamp(reportDTO.getTimestamp());
        report.setByUser(userRepository.findById(reportDTO.getByUser().getId()).orElse(null));
        report.setPost(postRepository.findById(reportDTO.getPost().getId()).orElse(null));
        report.setComment(commentRepository.findById(reportDTO.getComment().getId()).orElse(null));
        return report;
    }
    
}
