package ftn.sf.reddit.service.interfaces;

import java.time.LocalDate;
import java.util.List;

import ftn.sf.reddit.dto.BannedDTO;
import ftn.sf.reddit.dto.CommunityDTO;
import ftn.sf.reddit.dto.ModeratorDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Banned;

public interface BannedServiceInterface {
    
    public BannedDTO findOne(Long id);
    public List<BannedDTO> findAll();
    public BannedDTO findByUser(UserDTO userDTO);
    public List<BannedDTO> findByBannedFrom(CommunityDTO communityDTO);
    public List<BannedDTO> findByBy(ModeratorDTO moderatorDTO);
    public List<BannedDTO> findByTimestamp(LocalDate timestamp);
    public void save(BannedDTO bannedDTO);
    public void update(BannedDTO bannedDTO);
    public void remove(Long id);
    public Banned populateBanned(BannedDTO bannedDTO, Banned banned);
}

