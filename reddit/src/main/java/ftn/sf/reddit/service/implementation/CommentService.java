package ftn.sf.reddit.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.sf.reddit.dto.CommentDTO;
import ftn.sf.reddit.dto.PostDTO;
import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.Comment;
import ftn.sf.reddit.model.User;
import ftn.sf.reddit.repository.CommentRepository;
import ftn.sf.reddit.repository.PostRepository;
import ftn.sf.reddit.repository.UserRepository;
import ftn.sf.reddit.service.interfaces.CommentServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@RequiredArgsConstructor
@Log4j2
public class CommentService implements CommentServiceInterface {
    
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository<User> userRepository;
    @Autowired
    private PostRepository postRepository;

    @Override
    public CommentDTO findOne(Long id) {
        log.info("find by id {}", id);
        return new CommentDTO(commentRepository.findById(id).orElse(null));
    }

    @Override
    public List<CommentDTO> findAll() {
        log.info("find all comments");
        List<Comment> comments = commentRepository.findAll();
        List<CommentDTO> dtos = new ArrayList<>();
        for(Comment c : comments){
            dtos.add(new CommentDTO(c));
        }
        return dtos;
    }

    @Override
    public List<CommentDTO> findByRepliedTo(CommentDTO commentDTO) {
        log.info("find all replies for comments {}", commentDTO.getId());
        List<Comment> comments = commentRepository.findByRepliedTo(commentDTO);
        List<CommentDTO> dtos = new ArrayList<>();
        for(Comment c : comments){
            dtos.add(new CommentDTO(c));
        }
        return dtos;
    }

    @Override
    public List<CommentDTO> findByAuthor(UserDTO userDTO) {
        log.info("find all comments from one person {}", userDTO.getUsername());
        List<Comment> comments = commentRepository.findByAuthor(userRepository.getById(userDTO.getId()));
        List<CommentDTO> dtos = new ArrayList<>();
        for(Comment c : comments){
            dtos.add(new CommentDTO(c));
        }
        return dtos;
    }

    @Override
    public List<CommentDTO> findByPost(PostDTO postDTO) {
        log.info("find all comments for one post {}", postDTO.getTitle());
        List<Comment> comments = commentRepository.findByPost(postRepository.getById(postDTO.getId()));
        List<CommentDTO> dtos = new ArrayList<>();
        for(Comment c : comments){
            dtos.add(new CommentDTO(c));
        }
        return dtos;
    }

    @Override
    public void save(CommentDTO commentDTO) {
        commentRepository.save(populateComment(commentDTO, new Comment()));
        log.info("saved comment {}", commentDTO.getText());
    }

    @Override
    public void update(CommentDTO commentDTO) {
        Comment comment = commentRepository.findById(commentDTO.getId()).orElse(null);
        if(comment != null){
            commentRepository.save(populateComment(commentDTO, new Comment()));
            log.info("updated comment {}", commentDTO.getText());
        }
        
    }

    @Override
    public void remove(Long id) {
        commentRepository.deleteById(id);
        log.info("deleted by id {}", id);
        
    }

    @Override
    public Comment populateComment(CommentDTO comment, Comment comment2) {
        comment2.setAuthor(userRepository.findById(comment.getAuthor().getId()).orElse(null));
        comment2.setDeleted(comment.isDeleted());
        comment2.setPost(postRepository.findById(comment.getPost().getId()).orElse(null));
        comment2.setRepliedTo(commentRepository.findById(comment.getRepliedTo().getId()).orElse(null));
        Set<Comment> replies = new HashSet<>();
        Set<CommentDTO> dtos = comment.getReplies();
        for(CommentDTO c : dtos){
            replies.add(commentRepository.getById(c.getId()));
        }
        comment2.setReplies(replies);
        comment2.setText(comment.getText());
        comment2.setTimestamp(comment.getTimestamp());

        return comment2;
    }

    @Override
    public Long calculateKarma(Long id, Long id2) {
        return commentRepository.calculateKarma(id, id2);
    }
    
}
