package ftn.sf.reddit.service.interfaces;

import ftn.sf.reddit.dto.UserDTO;
import ftn.sf.reddit.model.User;

public interface UserService {
    
    UserDTO findById(Long id);
    
    UserDTO findByUsername(String username);

    UserDTO createUser(UserDTO userDTO);

    UserDTO update(UserDTO userDTO);

    String getRole(Long id);

    User populateUser(UserDTO userDTO, User user);

    UserDTO updatePassword(UserDTO userDTO, String oldpassword, String newPassword);

    Long calculateKarma(Long id, Long id2);
}
