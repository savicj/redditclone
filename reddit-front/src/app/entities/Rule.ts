import { Community } from "./Community";

export interface Rule {
    id? : number,
	description : string,
	community : Community
}