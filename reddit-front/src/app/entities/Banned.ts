import { Community } from "./Community";
import { Moderator } from "./Moderator";
import { User } from "./User";

export interface Banned {
    id? : number,
    timestamp : string,
    by : Moderator[],
    bannedFrom : Community,
    user : User
}