import { Banned } from "./Banned";
import { Flair } from "./Flair";
import { Moderator } from "./Moderator";
import { Post } from "./Post";
import { User } from "./User";

export interface Community {
    id? : number,
	name : string, 
	description : string,
	isSuspended : boolean,
	suspendedReason : string,
	members : User[],
	moderatedBy : Moderator[],
	posts : Post[],
	flairs : Flair[],
	banned : Banned[],
}