import { Post } from "./Post";
import { User } from "./User";

export interface Reaction{
    id? : number,
    reactionType : string,
    timestamp : string, 
    post : Post,
    comment : Comment,
    reactedBy : User
}