import { Community } from "./Community";
import { Flair } from "./Flair";
import { User } from "./User";

export interface Post {
    id? : number,
    title : string, 
    text : string, 
    creationDate : string,
    imagePath? : string,
    author : User,
    flair? : Flair,
    community : Community,
    comments : Comment[]
}