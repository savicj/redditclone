import { Post } from "./Post";

export interface KarmaPost{
    postDTO : Post,
    karma : number
}