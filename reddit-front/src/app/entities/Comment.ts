import { Post } from "./Post";
import { User } from "./User";

export interface Comment{
    id? : number,
    text : string,
    timestamp : string,
    isDeleted : boolean,
    replies : Comment[],
    repliedTo : Comment,
    author : User,
    post : Post
}