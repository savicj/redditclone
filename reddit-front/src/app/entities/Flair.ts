import { Community } from "./Community";

export interface Flair{
    id? : number,
    name : string, 
    community? : Community[] 
}