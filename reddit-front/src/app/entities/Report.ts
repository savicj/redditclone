import { Post } from "./Post";
import { User } from "./User";

export interface Report {
    id? : number,
    reason : string,
    timestamp : string,
    accepted : boolean,
    by : User,
    comment : Comment,
    post : Post
}