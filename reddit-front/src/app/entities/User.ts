import { Community } from "./Community";

export interface User {
    id? : number,
	username : string, 
	password : string,
	email : string
    avatar? : string,
    registrationDate : string,
    description : string,
    displayName : string, 
    community : Community[]
}