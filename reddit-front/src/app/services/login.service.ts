import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

const httpOptionsLogin = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  responseType: 'text' as 'json',
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private apiUrl = 'http://localhost:8080/reddit/users/login';
  helper = new JwtHelperService();

  constructor(private http: HttpClient) { }

  tryLogin(username: string, password: string): Observable<any> {
    const url = `${this.apiUrl}`;
    return this.http.post(
      url,
      { username: username, password: password },
      httpOptionsLogin
    );
  }

  getDecodedToken() {
    const token = localStorage.getItem('access_token');
    const decodedToken = token !== null ? this.helper.decodeToken(token) : null;
    return decodedToken;
  }

  logout() {
    localStorage.removeItem('access_token');

  }
}
