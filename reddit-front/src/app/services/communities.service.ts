import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { Banned } from '../entities/Banned';
import { Community } from '../entities/Community';
import { KarmaPost } from '../entities/KarmaPost';
import { Moderator } from '../entities/Moderator';
import { User } from '../entities/User';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};
const httpOptionsAuth = {
  headers: new HttpHeaders({
    'Accept': 'application/json',
    'Authorization' : 'Bearer ' + localStorage.getItem("access_token")  
  })
}

@Injectable({
  providedIn: 'root'
})
export class CommunitiesService {
  private apiUrl = 'http://localhost:8080/reddit/communities';
  helper = new JwtHelperService();

  constructor(private http: HttpClient) {}


  getUnsuspendedCommunities() : Observable<Community[]>{
    return this.http.get<Community[]>(`${this.apiUrl}/unsuspended`);
  }

  getCommunity(id : number) : Observable<Community> {
    return this.http.get<Community>(`${this.apiUrl}/${id}`);
  }

  getPosts(id : number) : Observable<KarmaPost[]>{
    return this.http.get<KarmaPost[]>(`${this.apiUrl}/${id}/posts`);
  }

  getMembers(id : number) : Observable<User[]>{
    return this.http.get<User[]>(`${this.apiUrl}/${id}/members`);
  }

  getModerators(id : number) : Observable<Moderator[]>{
    return this.http.get<Moderator[]>(`${this.apiUrl}/${id}/moderators`);
  }

  getBanned(id : number) : Observable<Banned[]>{
    return this.http.get<Banned[]>(`${this.apiUrl}/${id}/banned`);
  }

  suspend(id : number, reason: string) {
    const url = `${this.apiUrl}/${id}/suspend`;
    return this.http.post<HttpResponse<any>>(url, {'suspendedReason' : reason}, httpOptionsAuth);

  }

  edit(id: number, community: Community){
    const url = `${this.apiUrl}/${id}/update`;
    return this.http.post(url, community, httpOptionsAuth);
  }

  save(community : any){
    return this.http.put(`${this.apiUrl}/create`, community, httpOptionsAuth);
  }
  
}
