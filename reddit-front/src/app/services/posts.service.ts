import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { KarmaPost } from '../entities/KarmaPost';
import { Post } from '../entities/Post';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

const httpOptionsAuth = {
  headers: new HttpHeaders({
    'Accept': 'application/json',
    'Authorization' : 'Bearer ' + localStorage.getItem("access_token")  
  })
}

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private apiUrl = 'http://localhost:8080/reddit/posts';
  helper = new JwtHelperService();

  constructor(private http: HttpClient) {}

  getPosts() : Observable<KarmaPost[]>{
    return this.http.get<KarmaPost[]>(`${this.apiUrl}`);
  }

  upvote(id : number) {
    const url = `${this.apiUrl}/${id}/upvote`;
    return this.http.put(url, null, httpOptionsAuth);
  }

  downvote(id : number) {
    const url = `${this.apiUrl}/${id}/downvote`;
    return this.http.put(url, null, httpOptionsAuth);
  }

  create(post : Post) {
    return this.http.put<HttpResponse<any>>(`${this.apiUrl}`, post, httpOptionsAuth);
  }

  deletePost(id : any){
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptionsAuth);
  }
}
