import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { Community } from '../entities/Community';
import { User } from '../entities/User';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private apiUrl = 'http://localhost:8080/reddit/users';
  helper = new JwtHelperService();

  constructor(private http: HttpClient) {}

  getByUsername(username : string) : Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/${username}`);
  }

  getCommunitiesForUser(username : string) : Observable<Community[]>{
    return this.http.get<Community[]>(`${this.apiUrl}/${username}/communities`);
  }
  
}
