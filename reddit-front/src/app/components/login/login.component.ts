import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login() {
    this.loginService
      .tryLogin(this.username, this.password)
      .subscribe((response) => {
        const accessToken = response.toString();
        localStorage.setItem('access_token', accessToken);
        this.router.navigate(['/']);
      });
  }
}
