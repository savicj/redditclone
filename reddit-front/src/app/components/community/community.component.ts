import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Community } from 'src/app/entities/Community';
import { KarmaPost } from 'src/app/entities/KarmaPost';
import { Moderator } from 'src/app/entities/Moderator';
import { Post } from 'src/app/entities/Post';
import { User } from 'src/app/entities/User';
import { CommunitiesService } from 'src/app/services/communities.service';
import { LoginService } from 'src/app/services/login.service';
import { PostsService } from 'src/app/services/posts.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})
export class CommunityComponent implements OnInit {
  username : string;
  user : User;
  role : string;
  posts : KarmaPost[] = [];
  postKarma : KarmaPost;
  members : User[];
  moderators : Moderator[];
  id : any; 
  community : Community;
  isModerator : boolean;
  fb : FormGroup;
  editform : FormGroup;
  reason : string;

  constructor(
    private route : ActivatedRoute,
    private communitiesService : CommunitiesService,
    private postsService : PostsService,
    private loginService : LoginService,
    private userService : UsersService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void {
     this.fb = this.formBuilder.group({
      suspendedReason: '',
    });
    this.editform = this.formBuilder.group({
      name: '',
      desctiption : ''
    });

    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });    
    let decodedToken = this.loginService.getDecodedToken();

    if(decodedToken != null){
      this.role = decodedToken.role.authority;
      this.username = decodedToken.sub;
    }
    this.loadCommunity(Number(this.id));
    this.loadPosts(Number(this.id));
    this.loadMembers(Number(this.id));
    this.loadModerators(Number(this.id));
    
  }

  loadCommunity(id : number){
    this.communitiesService.getCommunity(id)
      .subscribe((community) => {
        this.community = community;
      })
    }
    
    loadMembers(id : number){
      this.communitiesService.getMembers(id)
      .subscribe((members) => {
        this.members = members;
      });
  }

  loadModerators(id : number){
    this.communitiesService.getModerators(id)
      .subscribe((moderators) => {
        this.moderators = moderators;
        if(this.username){
          this.checkIfLoggedUserIsModerator(this.username);   
        }
      });
  }

  loadPosts(id : number){
    this.communitiesService.getPosts(id)
      .subscribe((postskarmas) => {
        postskarmas.forEach((postkarma) =>{
          let karma = postkarma.karma;
          let post : Post = postkarma.postDTO;
          this.postKarma = <KarmaPost>({
            karma : karma,
            postDTO : post
          });
          this.posts.push(this.postKarma);
        })
      })
  }

  upvote(post : Post){
    if(this.username != null){
      let id : number = 0;
      post.id? (id = post.id) : (window.alert("id not valid"));
      this.postsService.upvote(id).subscribe();
      window.location.reload();
    }else{
      window.alert("log in first")
    }
    
  }

  downvote(post : Post){
    if(this.username != null){
      let id : number = 0;
      post.id? (id = post.id) : (window.alert("id not valid"));
      this.postsService.downvote(id).subscribe();
      window.location.reload();
    }else{
      window.alert("log in first")
    }
  }

  checkIfLoggedUserIsModerator(username : string) {
    this.userService.getByUsername(username)
    .subscribe((user) =>{
      for(let m of this.moderators){
        if(m.username === username){
          this.isModerator = true;
        }
      }
    });
    
  }

  suspendReasonInputShow(){
    document.getElementById('suspendBtn')?.setAttribute('style', 'display:none;');
    document.getElementById('suspend-div')?.removeAttribute('style');
  };

  suspend(value: any) {
    this.fb.get('suspendedReason')?.setValue(value.suspendedReason);
    this.communitiesService.suspend(this.id, this.fb.get('suspendedReason')?.value  )
    .subscribe(response => {
      window.location.replace("/communities")
    })
  }

  edit(){
   this.communitiesService.edit(this.id, this.community)
   .subscribe((res) => {
    console.log(res);
   })
  }



  deletePost(post : Post) {
    if(window.confirm("Are you sure?")){
      console.log("confirmed"+ post.id)
      this.postsService.deletePost(post.id).subscribe(() =>{window.location.reload();});
    }
  }

  
}
