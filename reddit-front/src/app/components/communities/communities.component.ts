import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Community } from 'src/app/entities/Community';
import { Moderator } from 'src/app/entities/Moderator';
import { User } from 'src/app/entities/User';
import { CommunitiesService } from 'src/app/services/communities.service';
import { LoginService } from 'src/app/services/login.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.css']
})
export class CommunitiesComponent implements OnInit {
  username : string;
  role : string;
  user : User;
  communities : Community[] = [];
  community : Community;
  emptyCommunity : Community = {
    name: '',
    description: '',
    isSuspended: false,
    suspendedReason: '',
    members: [],
    moderatedBy: [],
    posts: [],
    flairs: [],
    banned: []
  };
  constructor(
    public jwtHelper: JwtHelperService,
    private communityService : CommunitiesService,
    private loginService : LoginService,
    private userService : UsersService
  ) { }

  ngOnInit(): void {
    const decodedToken = this.loginService.getDecodedToken();
    if(decodedToken !== null){
      this.role = decodedToken.role.authority;
      this.userService.getByUsername(decodedToken.sub).subscribe((user) => {this.user = user;});
    }else{
      this.role = ''
    }
    this.loadCommunities();
  }

  loadCommunities(){
    this.communityService
      .getUnsuspendedCommunities()
      .subscribe((communities) => {
        this.communities = communities;
      });
  }

  createFormShow(){
    document.getElementById("create_community-div")?.removeAttribute("style");
    document.getElementById("createbtn")?.setAttribute("style", "display:none;");
  }

  create(){
    this.emptyCommunity.members.push(this.user);
    if(this.role != 'ROLE_MODERATOR'){
      let moderates : Community[] = [];
      moderates.push(this.emptyCommunity);
      let moderator : Moderator = {
        id: this.user.id,
        username: this.user.username,
        password: this.user.password,
        email: this.user.email,
        registrationDate: this.user.registrationDate,
        description: this.user.description,
        displayName: this.user.displayName,
        community: this.user.community,
        avatar: ''
      }
      this.emptyCommunity.moderatedBy.push(moderator);
      console.log(this.emptyCommunity)
      this.communityService.save(this.emptyCommunity).subscribe();
    }else {
      console.log("vec je moderator ah")
    }
  }
}
