import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  role : string;
  constructor(
    private jwtHelper: JwtHelperService,
    private router: Router,
    private loginService : LoginService

  ) { }

  ngOnInit(): void {

    let decodedToken = this.loginService.getDecodedToken();

    decodedToken !== null
      ? (this.role = decodedToken.role.authority)
      : (this.role = "unregistered");
    
  }

  logout(){
    this.loginService.logout();
    window.location.reload();
  }
}
