import { Component, OnInit } from '@angular/core';
import { Community } from 'src/app/entities/Community';
import { KarmaPost } from 'src/app/entities/KarmaPost';
import { Post } from 'src/app/entities/Post';
import { User } from 'src/app/entities/User';
import { LoginService } from 'src/app/services/login.service';
import { PostsService } from 'src/app/services/posts.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  username : string;
  role : string;
  user : User;
  post : Post;
  emptyPost : Post ;
  posts : KarmaPost[] = [];
  postKarma : KarmaPost;
  communities : Community[] = [];

  constructor(
    private postsService : PostsService,
    private loginService : LoginService,
    private userService : UsersService
  ) { }

  ngOnInit(): void {

    let decodedToken = this.loginService.getDecodedToken();
    if(decodedToken != null){
      this.role = decodedToken.role.authority;
      this.username = decodedToken.sub;
      this.userService.getByUsername(this.username)
      .subscribe((user)=>{
        this.user = user
        this.emptyPost = {
          title: '',
          text: '',
          creationDate: '',
          imagePath: '',
          author: this.user,
          // flair: {
          //   id : 0,
          //   name : ''
          // },
          community: {
            id : 0,
            name : '', 
            description : '',
            isSuspended : false,
            suspendedReason : '',
            members : [],
            moderatedBy : [],
            posts : [],
            flairs : [],
            banned : [],
          },
          comments: []
        }
      });
    }else{
      this.role = "unregistered";
    }    

    this.loadPosts();
  }

  loadPosts(){
    this.postsService
    .getPosts()
    .subscribe((postskarmas) => {
      postskarmas.forEach((postkarma) =>{
        let karma = postkarma.karma;
        let post : Post = postkarma.postDTO;
        this.postKarma = <KarmaPost>({
          karma : karma,
          postDTO : post
        });
        this.posts.push(this.postKarma);
      })
    });
  }

  createPostFormShow(){
    const form = document.getElementById("create_post-div");
    form?.removeAttribute("style");
    document.getElementById('createbtn')?.setAttribute('style', 'display:none;')
    const btn = document.getElementById("createbtn");
    // btn?.setAttribute("style", "'display=none;'");
    btn?.classList.add("d-none");
    this.findCommunitiesForUser();
  }

  findCommunitiesForUser(){
    this.userService.getCommunitiesForUser(this.username)
    .subscribe((communities) => {
      this.communities = communities;
    })
  }

  create(){
    console.log(this.emptyPost);
    this.postsService.create(this.emptyPost).subscribe((res) => {
      window.location.reload();
    })    
  }

  upvote(post : Post){
    let id : number = 0;
    console.log(post.id);
    post.id? (id = post.id) : (window.alert("id not valid"));
    this.postsService.upvote(id).subscribe();
    window.location.reload();
    
  }

  downvote(post : Post){
    let id : number = 0;
    console.log(post.id);
    post.id? (id = post.id) : (window.alert("id not valid"));
    this.postsService.downvote(id).subscribe();
    window.location.reload();
  }


  
}
