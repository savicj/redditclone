import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { JwtModule } from '@auth0/angular-jwt';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommunitiesComponent } from './components/communities/communities.component';
import { PostsComponent } from './components/posts/posts.component';
import { PostComponent } from './components/post/post.component';
import { CommunityComponent } from './components/community/community.component';
import { RegisterComponent } from './components/register/register.component';

const appRoutes: Routes = [
  { path: '', component: PostsComponent},
  { path: 'communities', component: CommunitiesComponent },
  { path: 'communities/:id', component: CommunityComponent },
  { path: 'login', component: LoginComponent},
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PostsComponent,
    NotFoundComponent,
    NavBarComponent,
    FooterComponent,
    CommunitiesComponent,
    CommunityComponent,
    PostComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:8080'],
        disallowedRoutes: ['http://localhost:8080/reddit/users/login',
                          'http://localhost:8080/reddit/posts',
                          'http://localhost:8080/reddit/communities/unsuspended',
                          'http://localhost:8080/reddit/communities/*'
                          // 'http://localhost:8080/reddit/communities/**/*'
        ],
        skipWhenExpired: true,
      },
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

